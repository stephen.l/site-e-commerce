import React, {useState,useEffect} from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
import jwt from "jsonwebtoken";
import $ from 'jquery';

import {validateMail} from '../validators/clientValidator';

const Login = (props) => {
    const [connectClient] = useState({email: null,password: null});
    const {cookie, setCookie, setExpToken} = props;

    const [MailIsValid,setMailIsValid] = useState(false);
    const [passwd,setPasswd] = useState(false);
    const [errs,setErrs] = useState([]);

    useEffect(() => {
        if(errs.length>0){
          $('#errModal').modal('toggle');
        }
    }, [errs]);

    const handleSubmit = e => {
        e.preventDefault();
        (async function connectionClient(){
            setErrs([]);
            try {
              const res = await axios.post(props.url+`/login`,connectClient);
              if (res.status === 200){
                  const payload = jwt.decode(res.data.token);
                  setCookie('payload',payload,{sameSite:'lax'});
                  setCookie('token',res.data.token,{sameSite:'lax'});
                  setCookie('bagLines',[],{sameSite:'lax'});
                  const expireIn = payload.exp - (Date.now() / 1000);
                  setExpToken(expireIn);
              }
            } catch (e) {
              setErrs([e.response.data]);
            }
        })();

    }

    const render = (
        <div className="container mt-5 pt-5 col-xl-4 col-md-6 col-sm-8">
          <h1>Connexion</h1>
            {cookie.payload ? <Redirect to={"/me"}/> :
            <div>

              <div className="modal fade" id="errModal" tabIndex="-1" role="dialog" aria-labelledby="errModalLabel" data-backdrop="static" aria-hidden="true">
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title text-danger" id="errModalLabel"> Erreur(s) </h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div className="modal-body">
                      {
                        errs.length > 0 ?
                        <ul className="list-group list-group-flush">
                          {
                            errs.map(e =>
                              <li className="list-group-item" key={e}>{e}</li>
                            )
                          }
                        </ul>
                        :
                        <div></div>
                      }
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-outline-primary" data-dismiss="modal">Fermer</button>
                  </div>
                </div>
              </div>
              </div>


              <form onSubmit={handleSubmit}>
                <div className="form-group ">
                  <input type="email" className={MailIsValid ? "form-control is-valid":"form-control is-invalid"} placeholder="Email" onChange={e =>
                      {
                        connectClient.email = e.target.value;
                        setMailIsValid(validateMail(connectClient.email));
                      }
                    }
                  />
                  {
                    !MailIsValid ?
                    <small className="form-text text-danger m-1 mb-2">Mail non valide</small>
                    :
                    <div></div>
                  }
                </div>
                <div className="form-group">
                  <input type="password" className={passwd ? "form-control is-valid":"form-control is-invalid"} id="exampleInputPassword1" placeholder="Mot de passe" onChange={e =>
                      {
                        connectClient.password = e.target.value;
                        setPasswd((connectClient.password.length>0));
                      }
                    }
                  />
                  {
                    !passwd ?
                    <small className="form-text text-danger m-1 mb-2">Mot de passe requis</small>
                    :
                    <div></div>
                  }
                </div>

                {
                  MailIsValid && passwd ?
                  <button type="submit" className="btn btn-primary rounded-pill">Connexion </button>
                  :
                  <span className="btn btn-primary disabled rounded-pill">Connexion </span>
                }
              </form>
            </div>

            }
        </div>
    );

    return render;
}

export default Login;
