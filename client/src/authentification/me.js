import React from "react";
import {Redirect} from 'react-router-dom';

import ShowClient from "../clientsComponents/showClient";

const Me = (props) => {
    const {config, cookie} = props;

    return (
      <div>
      {
        cookie.payload ?
        <div>
            {!cookie.payload ? <Redirect to={"/"}/> :
                <div>
                    <ShowClient id={cookie.payload.id} config={config} cookie={cookie} url={props.url} />
                </div>
          }
        </div>
        :
        <Redirect to={"/"}/>
      }
      </div>
    );
};

export default Me;
