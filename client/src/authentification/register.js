import React, {useState,useEffect} from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
import $ from 'jquery';

import ClientForm from "../clientsComponents/clientForm";
import {validateClient} from '../validators/clientValidator';

const Register = (props) => {
  const {cookie} = props;
  const [newClient] = useState({last_name: null,first_name: null,email: null,password: null,address: null});
  const [redirect, setRedirect] = useState(false);
  const [errs,setErrs] = useState([]);

  const validate = (client) => {
    return validateClient(client);
  }

  const handleSubmit = e => {
      e.preventDefault();
      (async function nouveauClient(){
          setErrs([]);
          try {
            const res = await axios.post(props.url+`/register`,newClient);
            if (res.status === 200){
                setRedirect(true);
            }
          } catch (e) {
            let tab =[];
            e.response.data.errors.forEach(er => tab.push(Object.values(er)));
            setErrs(tab);
          }
      })();
  }

  useEffect(() => {
      if(errs.length>0){
        $('#errModal').modal('toggle');
      }
  }, [errs]);

    return (
        <div>
            {
                cookie.payload ?
                <Redirect to={"/"}/>
                :
                <div className="container mt-5 pt-5">
                  <h1>Création d'un compte</h1>

                    <div className="modal fade" id="errModal" tabIndex="-1" role="dialog" aria-labelledby="errModalLabel" data-backdrop="static" aria-hidden="true">
                    <div className="modal-dialog">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h5 className="modal-title text-danger" id="errModalLabel"> Erreur(s) </h5>
                          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div className="modal-body">
                            {
                              errs.length > 0 ?
                              <ul className="list-group list-group-flush">
                                {
                                  errs.map(e =>
                                    <li className="list-group-item" key={e}>{e}</li>
                                  )
                                }
                              </ul>
                              :
                              <div></div>
                            }
                        </div>
                        <div className="modal-footer">
                          <button type="button" className="btn btn-outline-primary" data-dismiss="modal">Fermer</button>
                        </div>
                      </div>
                    </div>
                    </div>

                  <ClientForm
                    client={newClient}
                    handleSubmit={handleSubmit}
                    isUpdate={false}
                    validate={validate}
                  />
                {redirect && (<Redirect to="/login"/>)}
                </div>
            }
        </div>
    )
}

export default Register;
