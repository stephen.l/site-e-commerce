import React, {useEffect, useState} from 'react';
import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPhone,faEnvelope} from '@fortawesome/free-solid-svg-icons';

import {BrowserRouter as Router, Route,Switch,Link} from "react-router-dom";
import { useCookies } from 'react-cookie';

import Navigation from "./Navigation";
import Home from "./Home";

import Register from "./authentification/register";
import Login from "./authentification/login";
import Me from "./authentification/me";

import ListClients from "./clientsComponents/listClient";
import ShowClientAdmin from "./clientsComponents/showClientAdmin";
import UpdateClient from "./clientsComponents/updateClient";
import ClientOrders from "./clientsComponents/clientOrders";

import ListProducts from "./productsComponents/listProduct";
import ShowProduct from "./productsComponents/showProduct";
import CreateProduct from "./productsComponents/createProduct";
import UpdateProduct from "./productsComponents/updateProduct";

import ListOrders from './ordersComponents/listOrder';
import OrderBag from './ordersComponents/orderBag';

import PageNotFound from './pageNotFound';



const App = () => {
    const [cookie, setCookie, removeCookie] = useCookies(['payload','token','bagLines']);
    const [expToken, setExpToken] = useState(0);

    const url = "http://localhost:8000";
    const config = {
        headers: {
          'accept': 'application/json',
          'Authorization': `Bearer ${cookie.token}`
        }
    };

    useEffect(() => {
        if (expToken !== 0) {
            const timer = setTimeout(() => {
                removeCookie('payload');
                removeCookie('token');
                setExpToken(0);
            },expToken*1000);
            return () => clearTimeout(timer);
        }
    }, [expToken, removeCookie]);

    return (
        <div>
            <Router>
            <Navigation cookie={cookie} setExpToken={setExpToken} url={url} removeCookie={removeCookie} />
            <Switch>
                <Route exact path="/" render={(props) => <Home cookie={cookie} config={config} url={url} {...props}/>}/>

                <Route exact path="/register" render={(props) => <Register cookie={cookie} url={url} {...props} /> }/>
                <Route exact path="/login" render={(props) => <Login cookie={cookie} setCookie={setCookie} url={url} setExpToken={setExpToken} {...props} /> }/>
                <Route exact path="/me" render={(props) => <Me cookie={cookie} config={config} url={url} {...props} /> }/>

                <Route exact path="/clients" render={(props) => <ListClients config={config} cookie={cookie} url={url} {...props} /> } />
                <Route exact path="/clients/search=:search" render={(props) => <ListClients config={config} cookie={cookie} url={url} {...props} /> } />
                <Route exact path="/clients/update/:id" render={(props) => <UpdateClient config={config} cookie={cookie} url={url} {...props} /> }/>
                <Route exact path="/clients/:id/orders" render={(props) => <ClientOrders config={config} cookie={cookie} url={url} {...props} /> }/>
                <Route path="/clients/:id" render={(props) => <ShowClientAdmin config={config} cookie={cookie} url={url} {...props} /> }/>

                <Route exact path="/products" render={(props) => <ListProducts config={config} cookie={cookie}  url={url} {...props} /> }/>
                <Route exact path="/products/create" render={(props) => <CreateProduct config={config} cookie={cookie} url={url} {...props} /> }/>
                <Route exact path="/products/:ref/update" render={(props) => <UpdateProduct config={config} cookie={cookie} url={url} {...props} /> }/>
                <Route exact path="/products/search=:search" render={(props) => <ListProducts config={config} cookie={cookie} url={url} {...props} /> }/>
                <Route path="/products/:ref" render={(props) => <ShowProduct config={config} cookie={cookie} url={url} {...props}/> }/>

                <Route exact path="/orders" render={(props) => <ListOrders config={config} cookie={cookie} url={url} {...props} /> } />
                <Route exact path="/orders/search=:search" render={(props) => <ListOrders config={config} cookie={cookie} url={url} {...props} /> } />
                <Route exact path="/bag" render={(props) => <OrderBag config={config} cookie={cookie} url={url} {...props} /> } />

              <Route component={PageNotFound}/>
            </Switch>


            <footer className="page-footer font-small bg-dark text-white mt-4 pt-4">
              <div className="container text-center text-md-left">
                <div className="row">
                  <div className="col-md-4 col-lg-3 mr-auto my-md-4 my-0 mt-4 mb-1">
                    <h5 className="font-weight-bold text-uppercase mb-4">Qui sommes-nous ?</h5>
                    <p>Nous sommes des commerçants voulant vous proposez des produits divers et variés allant de simple vêtements à la nourriture en passant par des objets électroniques.</p>

                  </div>

                  <hr className="bg-white clearfix w-100 d-md-none"/>

                  <div className="col-md-2 col-lg-2 mx-auto my-md-4 my-0 mt-4 mb-1">
                    <h5 className="font-weight-bold text-uppercase mb-4">Liens</h5>

                    <ul className="list-unstyled">
                      <li>
                        <p>
                          <Link className="text-white" to={"/"}>Accueil</Link>
                        </p>
                      </li>
                      <li>
                        <p>
                          <Link className="text-white" to={"/products"}>Nos produits</Link>
                        </p>
                      </li>
                    </ul>

                  </div>

                  <hr className="bg-white clearfix w-100 d-md-none"/>
                  <div className="col-md-4 col-lg-3 mx-auto my-md-4 my-0 mt-4 mb-1">
                    <h5 className="font-weight-bold text-uppercase mb-4">Contact</h5>

                    <ul className="list-unstyled">
                      <li>
                        <p>
                          <FontAwesomeIcon className="mr-1" icon={faEnvelope}/> contact@lmls.fr</p>
                      </li>
                      <li>
                        <p>
                        <FontAwesomeIcon className="mr-1" icon={faPhone}/> 06 07 08 09 10</p>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="footer-copyright text-center py-3">© 2020 Copyright:
                LMLS Shopping
              </div>
            </footer>

            </Router>

        </div>
    )
}

export default App;
