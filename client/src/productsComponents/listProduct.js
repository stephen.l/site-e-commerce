import React, {useEffect, useState} from "react";
import axios from "axios";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAngleRight} from '@fortawesome/free-solid-svg-icons';

const Product = (props) => {
    const [product] = [props.p];

    return(
      <div>
        {
          [product].map(
            p =>
              <div key={p.ref} className=" card shadow m-4 justify-content-center">
                <div className="card-header text-center">
                <h5>{p.name}
                </h5>
                {
                  p.stock < p.stock_alert && p.stock > 0 ?
                    <span className="badge badge-warning">Bientot épuisé</span>
                  :
                  <div>
                  </div>
                }

                {
                  p.stock === 0 ?
                    <span className="badge badge-danger">En rupture de stock</span>
                  :
                  <div>
                  </div>
                }
              </div>
                <img src={props.url+"/"+p.url_img} className="card-img-top" alt="..." />
                <div className="card-body">

                  {
                    p.description !== null ?
                    <p className="lead"> {product.description} </p>
                    :
                    <span></span>
                  }
                  <Link to={`/products/${p.ref}`} className="btn btn-primary ">Voir le produit <FontAwesomeIcon className="ml-1" aria-hidden="true" icon={faAngleRight}/></Link>
                </div>
              </div>
          )
        }
      </div>
    );
}

const ListProducts = (props) => {
  const [params,setParams] = useState({offset:0,limit:9});
  const [loading,setLoading] = useState(true);
  const [products, setProducts] = useState([]);

  useEffect(() => {
      (async function getProducts() {
          const res = await axios.get(props.url+`/products/limit=${params.limit}-offset=${params.offset}`);
          if (res.status === 200) {
            setLoading(false);
            setProducts(res.data);
          }
      })();
  },[props.url,params]);

  const showMore = () => {
      setLoading(true);
      setParams({offset:0,limit:params.limit+20});
    };

  return (
      <div className="container mt-5 mb-5 pt-2">
        {
          props.match.params.search  ?
          <h1 className="text-center mt-5 pt-3">Résultat pour "{props.match.params.search}"</h1>
          :
          <h1 className="text-center mt-5 pt-3">Liste des produits</h1>
        }

      {loading ?
        <div className="text-center mt-3">
          <h1>Chargement..</h1>
            <div className="spinner-border text-primary" role="status">
              <span className="sr-only">Chargement...</span>
            </div>
        </div>

        :
        <div>

            <div className=" container justify-content-center">
              {
                !props.match.params.search ||
                (props.match.params.search &&
                products
                  .filter(
                    p =>
                      p.name.toLowerCase().split(' ').some(w=>w.startsWith(props.match.params.search.toLowerCase()))
                  )
                  .length > 0 ) ?
                <div className="container">
                  <div className="row row-cols-sm-1 row-cols-md-2 row-cols-lg-3 ">
                      {
                        props.match.params.search ?

                        products
                          .filter(
                            p =>
                              p.name.toLowerCase().split(' ').some(w=>w.startsWith(props.match.params.search.toLowerCase()))
                          )
                          .map(
                            product => <Product key={product.ref} url = {props.url} p={product}/>
                        )
                        :
                        products
                          .map(
                            product => <Product key={product.ref} url = {props.url} p={product} />
                        )
                      }

                    </div>
                    <div className="container mt-2 text-center">
                      {
                        products.length === params.limit ?
                        <button className="btn btn-success btn-md" onClick={showMore}>
                          Voir plus de produits.
                        </button>
                        :
                        <small className="text-secondary text-center">
                          Fin des produits
                        </small>
                      }
                    </div>
                  </div>
                :
                <div>
                  {
                    props.match.params.search  ?
                    <h1 className="text-center mt-5 pt-3">Résultat pour "{props.match.params.search}"</h1>
                    :
                    <h1 className="text-center mt-5 pt-3">Aucun produits</h1>
                  }
                </div>
              }

            </div>
        </div>
      }
      </div>
    );

}

export default ListProducts;
