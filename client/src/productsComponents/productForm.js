import React, {useState,useEffect} from "react";

import {validateName,validateInt,validatePrice,validateProduct, validateImg} from "../validators/productValidator";

const ProductForm = (props) => {
    const {product,handleSubmit,isUpdate} = props;
    const [oldValues,setOldValues] = useState({});
    const [fileName, setFileName] = useState('Choose File');
    useEffect(() => {
      setOldValues(
        {
          name: product.name,
          description: product.description,
          stock: product.stock,
          stock_alert: product.stock_alert,
          price: product.price,
          file: product.file
        }
      )
    },[product]);

    const [nameIsValid,setNameIsValid] = useState(isUpdate ? true : false);
    const [stockIsValid,setStockIsValid] = useState(isUpdate ? true : false);
    const [stockAlertIsValid,setStockAlertIsValid] = useState(isUpdate ? true : false);
    const [priceIsValid,setPriceIsValid] = useState(isUpdate ? true : false);
    const [imgIsValid, setImgIsValid] = useState(isUpdate ? true : false);
    const [isValid,setIsValid] = useState(isUpdate ? true : false);


    return(
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label>Nom du produit</label>
          <input className={nameIsValid ? "form-control is-valid":"form-control is-invalid"} type="text" placeholder={oldValues.name ? oldValues.name : "Nom"} onChange={e =>
              {
                product.name = e.target.value;
                if(isUpdate && e.target.value===""){
                  product.name = oldValues.name;
                }
                setNameIsValid(validateName(product.name));
                setIsValid(validateProduct(product));
              }
            }
          />
        {
          !nameIsValid ?
            <small className="form-text text-danger m-1 mb-2">Nom du produit requis</small>
          :
            <div></div>
        }
        </div>

        <div className="form-group">
          <label>Description du produit</label>
          <textarea className="form-control" placeholder={oldValues.description ? oldValues.description : "Description"} rows="4"onChange={e =>
              {
                product.description = e.target.value;
              }
            }
          >
          </textarea>
        </div>

        <div className="form-group">
          <label>Stock du produit</label>
          <input className={stockIsValid ? "form-control is-valid":"form-control is-invalid"} type="text" placeholder={oldValues.stock ? oldValues.stock : "Stock"} onChange={e =>
              {
                product.stock = e.target.value;
                if(isUpdate && e.target.value===""){
                  product.stock = oldValues.stock;
                }
                setStockIsValid(validateInt(product.stock));
                setIsValid(validateProduct(product));
              }
            }
          />
          {
            !stockIsValid ?
            <small className="form-text text-danger m-1 mb-2">Stock est requis et doit être positif</small>
              :
              <div></div>
          }
        </div>

        <div className="form-group">
          <label>Stock Alert</label>
          <input className={stockAlertIsValid ? "form-control is-valid":"form-control is-invalid"} type="text" placeholder={oldValues.stock_alert ? oldValues.stock_alert : "Stock Alert"} onChange={e =>
              {
                product.stock_alert = e.target.value;
                if(isUpdate && e.target.value===""){
                  product.stock_alert = oldValues.stock_alert;
                }
                setStockAlertIsValid(validateInt(product.stock_alert));
                setIsValid(validateProduct(product));
              }
            }
          />
          {
            !stockAlertIsValid ?
            <small className="form-text text-danger m-1 mb-2"> Stock alert est requis et doit être positif</small>
              :
              <div></div>
          }
        </div>

        <div className="form-group">
          <label>Prix du produit</label>
            <input className={priceIsValid ? "form-control is-valid":"form-control is-invalid"} type="text" placeholder={oldValues.price ? oldValues.price : "Prix"} onChange={e =>
                {
                  product.price = e.target.value;
                  if(isUpdate && e.target.value===""){
                    product.price = oldValues.price;
                  }
                  setPriceIsValid(validatePrice(product.price));
                  setIsValid(validateProduct(product));
                }
              }
            />
          {
            !priceIsValid ?
            <small className="form-text text-danger m-1 mb-2">Le prix est requis et doit être positif</small>
            :
            <div></div>
          }
        </div>

        <div className="custom-file mb-2">
          <input type="file" className={imgIsValid ? "custom-file-input is-valid":"custom-file-input is-invalid"} id="customFile"
          onChange={ e =>{
              product.file = e.target.files[0];
              setFileName(product.file.name);
              if(isUpdate && e.target.files===""){
                product.file = oldValues.file;
              }
              setImgIsValid(validateImg(product.file.name));
              setIsValid(validateProduct(product));
            }
          }/>
          <label className="custom-file-label" htmlFor="customFile">{fileName}</label>
        </div>

        <div>
          {
            isUpdate ?
            <button type="submit" className="btn btn-primary btn-md">
              Valider
            </button>
            :
            <div>
            {
              isValid?
              <button type="submit" className="btn btn-primary btn-md">
                Ajouter le produit
              </button>
              :
              <span className="btn btn-primary disabled  btn-md">
                Ajouter le produit
              </span>
            }
            </div>
          }
        </div>

      </form>

    )
}

export default ProductForm;
