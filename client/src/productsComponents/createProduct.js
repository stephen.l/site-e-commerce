import React, {useState,useEffect} from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
import FormData from 'form-data';
import $ from 'jquery';

import ProductForm from "./productForm";

const CreateProduct = (props) => {
    const [newProduct] = useState({name: null,description: null,stock: null,stock_alert: null,price: null, file: null});
    const [redirect, setRedirect] = useState(false);
    const {cookie} = props;
    const [errs,setErrs] = useState([]);

    useEffect(() => {
        if(errs.length>0){
          $('#errModal').modal('toggle');
        }
    }, [errs]);

    const handleSubmit = e => {
        e.preventDefault();
        (async function nouveauProduit(){
            setErrs([]);
            const formData = new FormData();
            formData.append('name', newProduct.name);
            formData.append('description', newProduct.description);
            formData.append('stock', newProduct.stock);
            formData.append('stock_alert', newProduct.stock_alert);
            formData.append('price', newProduct.price);
            formData.append('file', newProduct.file);
            try {

              const configImg = {
                  headers: {
                    'accept': 'application/json',
                    'Authorization': `Bearer ${cookie.token}`,
                    'Content-Type': 'multipart/form-data'
                  }
              };
              const res = await axios.post(props.url+`/products`,formData,configImg);
              if (res.status === 200){
                  setRedirect(true);
              }
            } catch (e) {
              setErrs([e.response.data]);
            }
        })();

    }

    return(
        <div className="container mt-5 pt-5">
          {!(cookie.payload && (cookie.payload.role === 'seller')) ? <Redirect to={"/"}/> :
            <div>
              <h1>Ajout d'un nouveau produit</h1>

                <div className="modal fade" id="errModal" tabIndex="-1" role="dialog" aria-labelledby="errModalLabel" data-backdrop="static" aria-hidden="true">
                <div className="modal-dialog">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title text-danger" id="errModalLabel"> Erreur(s) </h5>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div className="modal-body">
                        {
                          errs.length > 0 ?
                          <ul className="list-group list-group-flush">
                            {
                              errs.map(e =>
                                <li className="list-group-item" key={e}>{e}</li>
                              )
                            }
                          </ul>
                          :
                          <div></div>
                        }
                    </div>
                    <div className="modal-footer">
                      <button type="button" className="btn btn-outline-primary" data-dismiss="modal">Fermer</button>
                    </div>
                  </div>
                </div>
                </div>

              <ProductForm
                product={newProduct}
                handleSubmit={handleSubmit}
                isUpdate={false}
              />
              {redirect && (<Redirect to={"/products"}/>)}
            </div>
            }
        </div>

    )
}

export default CreateProduct;
