import React, {useEffect, useState} from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
import FormData from 'form-data';
import $ from 'jquery';

import ProductForm from "./productForm";

const UpdateProduct = (props) => {
    const [updateProduct,setUpdateProduct] = useState({name: null,description: null,stock: null,stock_alert: null,price: null, file: null});
    const [redirect, setRedirect] = useState(false);
    const {cookie} = props;
    const [errs,setErrs] = useState([]);

    useEffect(() => {
        if((cookie.payload && (cookie.payload.role === 'seller'))) {
            (async function modifProduct() {
                const res = await axios.get(props.url+`/products/${props.match.params.ref}`)
                if (res.status === 200) {
                    setUpdateProduct(res.data[0]);
                }
            })();
        }
    },[props,cookie]);

    useEffect(() => {
        if(errs.length>0){
          $('#errModal').modal('toggle');
        }
    }, [errs]);


    const handleSubmit = e => {
        e.preventDefault();
        (async function modifClient() {
          setErrs([]);
          const formData = new FormData();
          formData.append('name', updateProduct.name);
          formData.append('description', updateProduct.description);
          formData.append('stock', updateProduct.stock);
          formData.append('stock_alert', updateProduct.stock_alert);
          formData.append('price', updateProduct.price);
          formData.append('file', updateProduct.file);

          try {
            const configImg = {
                headers: {
                  'accept': 'application/json',
                  'Authorization': `Bearer ${cookie.token}`,
                  'Content-Type': 'multipart/form-data'
                }
            };

              const res = await axios.put(props.url+`/products/${props.match.params.ref}`,formData,configImg);
              if (res.status === 200){
                  setRedirect(true);
              }
          } catch (e) {;
            setErrs([e.response.data]);
          }
        })();
    }

    return(
        <div className="container mt-5 pt-5">
            {!(cookie.payload && (cookie.payload.role === 'seller')) ? <Redirect to={"/"}/> :
                <div>
                    <h1>Modification du Produit</h1>

                      <div className="modal fade" id="errModal" tabIndex="-1" role="dialog" aria-labelledby="errModalLabel" data-backdrop="static" aria-hidden="true">
                      <div className="modal-dialog">
                        <div className="modal-content">
                          <div className="modal-header">
                            <h5 className="modal-title text-danger" id="errModalLabel"> Erreur(s) </h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div className="modal-body">
                              {
                                errs.length > 0 ?
                                <ul className="list-group list-group-flush">
                                  {
                                    errs.map(e =>
                                      <li className="list-group-item" key={e}>{e}</li>
                                    )
                                  }
                                </ul>
                                :
                                <div></div>
                              }
                          </div>
                          <div className="modal-footer">
                            <button type="button" className="btn btn-outline-primary" data-dismiss="modal">Fermer</button>
                          </div>
                        </div>
                      </div>
                      </div>

                    <ProductForm
                      product={updateProduct}
                      handleSubmit={handleSubmit}
                      isUpdate={true}
                    />
                    {redirect && (<Redirect to={`/products/${props.match.params.ref}`}/>)}
                </div>
            }
        </div>
    )
}

export default UpdateProduct;
