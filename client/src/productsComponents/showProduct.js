import React, {useEffect, useState} from "react";
import axios from "axios";
import {Redirect,Link} from "react-router-dom";


const ShowProduct = (props) => {
    const [product, setProduct] = useState([]);
    const [redirect, setRedirect] = useState(false);
    const [redirectSeller, setRedirectSeller] = useState(false);
    const {cookie,config} = props;
    const [quantity,setQuantity] = useState(1);


    useEffect(() => {
        (async function getProduct() {
            const res = await axios.get(props.url+`/products/${props.match.params.ref}`)
            if (res.status === 200){
                setProduct(res.data);
            }
        })();

    },[props]);


    const handleDelete = (ref) => {
        (async function deleteProduct() {
            const res = await axios.delete(props.url+`/products/${ref}`,config);
            if (res.status === 200){
                setRedirectSeller(true);
            }
        })();
    }

    const handleOrder = (product,quantity) => {
        (async function createBagLine() {
            const price = quantity*product.price;
            const price_with_vat = price * 1.20;
            const bagLine = {order_ref: null, product_ref: product.ref, quantity: quantity, price: price, price_with_vat: price_with_vat};

            const find = cookie.bagLines.map(bl => bl.product_ref).indexOf(bagLine.product_ref);

            if (find!==-1) {
                cookie.bagLines.splice(find,1);
            }

            cookie.bagLines.push(bagLine);
            })();
            setRedirect(true);
    }

    return (
        <div className="container mt-5 pt-5">
            <div className="card shadow mb-3">
              <div className="row no-gutters">
                <div className="col-md-6">
                  {product.map(p=><img key={p.ref} src={props.url+"/"+p.url_img} className="card-img" alt="..."/>)}
                </div>

                <div className="col-md-6">
                  {product.map(product =>
                    <div key={product.ref} className="card-body">
                      <h1 className="card-title" key={product.ref}> {product.name}</h1>
                      <div className="card-text" >
                          {
                            product.description !== null ?
                            <p className="lead"> {product.description} </p>
                            :
                            <span></span>
                          }
                          <p className="lead">Prix : {product.price} euros</p>

                          {
                            product.stock>1 && (cookie.payload ? cookie.payload.role==="client" : true) ?
                            <div>
                              <h5>Quantité</h5>
                              <div className="input-group w-50 mb-3">
                                <div className="input-group-prepend" onClick={() =>{
                                    if(quantity>1){
                                    setQuantity(quantity-1);
                                  }}
                                  }>
                                  <button className="btn btn-outline-danger">-</button>
                                </div>
                                <input type="text" className="form-control" placeholder={quantity} aria-label="Quantité" readOnly/>
                              <div className="input-group-prepend" onClick={() =>{
                                    if(quantity < product.stock){
                                    setQuantity(quantity+1);
                                    }
                                  }
                                  }>
                                  <button className="btn btn-outline-success">+</button>
                                </div>
                              </div>
                            </div>
                            :
                            <div></div>
                          }

                            {
                              product.stock < product.stock_alert && product.stock > 0 ?
                              <div>
                                <h2><span className="badge badge-warning">Bientot épuisé</span></h2>
                                <p className="lead">{product.stock} examplaires restant.</p>
                              </div>
                              :
                              <div>
                              </div>
                            }

                            {
                              product.stock === 0 ?
                              <h2><span className="badge badge-danger"> En rupture de stock</span></h2>
                              :
                              <div>
                              </div>
                            }

                          {
                            cookie.payload && cookie.payload.role==="seller" ?
                            <div className="container m-0 p-0">
                              <p className="lead">{product.stock} examplaire(s) restant(s)</p>
                              <p className="lead">Stock Alert : {product.stock_alert}</p>
                              <Link to={`/products/${product.ref}/update`} className="btn btn-warning mr-2 mb-2">Modifier</Link>
                              <button className="btn btn-danger mb-2" onClick={handleDelete}>Supprimer</button>
                            </div>
                            :
                            <div></div>
                          }
                          {
                            cookie.payload ?
                            <div>
                              {
                                cookie.payload.role==="client" ?
                                <div className="container m-0 p-0">
                                  {
                                    product.stock > 0 ?
                                    <button className="btn btn-success mr-2 mb-2" onClick={() => handleOrder(product,quantity)} >Commander</button>
                                    :
                                    <span className="btn btn-success disabled mr-2 mb-2">Commander</span>
                                  }
                                </div>
                                :
                                <div></div>
                              }
                            </div>
                            :
                            <Link to={"/login"} className="btn btn-success mr-2 mb-2">Connectez vous pour commander</Link>
                          }
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
            {redirect && (<Redirect to={"/bag"}/>)}
            {redirectSeller && (<Redirect to={"/products"}/>)}
        </div>
    );
};

export default ShowProduct;
