import React, {useEffect, useState} from "react";
import axios from "axios";
import {Link, Redirect} from "react-router-dom";
import $ from 'jquery';

const OrderLine = (props) => {
  const {ol,handleDelete,refresh,setRefresh, refreshTotalPrice} = props;
  const [product,setProduct] = useState({});
  const [quantity] = useState({value: ol.quantity});

  useEffect(
    () => {
      (async function getProduct() {
        const res = await axios.get(props.url+`/products/${ol.product_ref}`);
        if(res.status===200) {
          setProduct(res.data[0]);
        }
      })();
    },[ol,props.url]);

    useEffect(() => {
    }, [refresh]);
  return(
      <tr>
        <td>{ol.product_ref}</td>
        <td><Link className="btn btn-link" to={`/products/${ol.product_ref}`}>{product.name}</Link></td>
        <td><div className="input-group mb-3">
          <div className="input-group-prepend" onClick={() =>{
              if(quantity.value > 1){
              quantity.value--;
              ol.quantity = quantity.value;
              ol.price = product.price * ol.quantity;
              ol.price_with_vat = ol.price * 1.20;
              refreshTotalPrice();
              if (refresh) {
                setRefresh(false);
              }
              else {
                setRefresh(true);
              }
            }}
            }>
            <button className="btn btn-outline-danger">-</button>
          </div>
          <input type="text" className="form-control" placeholder={ol.quantity} aria-label="Quantité" readOnly/>
        <div className="input-group-prepend" onClick={() =>{
              if(quantity.value < product.stock){
                quantity.value++;
                ol.quantity = quantity.value;
                ol.price = product.price * ol.quantity;
                ol.price_with_vat = ol.price * 1.20;
                refreshTotalPrice()
                if (refresh) {
                  setRefresh(false);
                }
                else {
                  setRefresh(true);
                }
              }
            }
            }>
            <button className="btn btn-outline-success">+</button>
          </div>
        </div>
        </td>
        <td>{ol.price}</td>
        <td>{ol.price_with_vat}</td>
        <td><button className="btn btn-danger" onClick={() => handleDelete(ol.product_ref)}>X</button></td>
      </tr>

  );
}


const BagOrder = (props) => {
  const{cookie,config}=props;
  const [client,setClient]=useState({});
  const [address,setAddress] = useState({value:null,oldValue:null});
  const [redirect,setRedirect] = useState(false);
  const [refresh,setRefresh] = useState(false);
  const [totalPrice] = useState({value: 0});
  const [errs,setErrs] = useState([]);

  useEffect(() => {
    if(cookie.payload) {
        (async function getClient() {
            const res = await axios.get(props.url+`/clients/${cookie.payload.id}`, config);
            if (res.status === 200){
                setClient(res.data[0]);
            }
        })();
    }
  },[config,cookie,props.url]);

  useEffect(() => {
    setAddress(
      {
        value:client.address,
        isValid:client.address ? true : false,
        oldValue:client.address
      }
    )
  },[client]);

  useEffect( () => {
    refreshTotalPrice();
  },[refresh]);

  useEffect(() => {
      if(errs.length>0){
        $('#errModal').modal('toggle');
      }
  }, [errs]);


  const deleteLine = (ref) => {
    const find = cookie.bagLines.map(bl => bl.product_ref).indexOf(ref);

    if (find!==-1) {
        cookie.bagLines.splice(find,1);
        if (refresh) {
          setRefresh(false);
        }
        else {
          setRefresh(true);
        }
    }
  }

  const refreshTotalPrice = () => {
    totalPrice.value = 0;
    cookie.bagLines.forEach(bagLine => totalPrice.value += bagLine.price_with_vat);
  }

  // const verifyQty = async () => {
  //   try {
  //     cookie.bagLines.forEach( l => {
  //       axios.get(props.url+`/products/${l.product_ref}`).then(res => {
  //         if(res.status===200) {
  //           if(res.data[0].stock < l.quantity){
  //             setErrs([`Le produit ${res.data[0].name} n'est pas disponible dans les quantités demandés. \n Ils ne restent que ${res.data[0].stock} exemplaires en stock`]);
  //             return false;
  //           }
  //         }
  //       });
  //
  //     })
  //     return true;
  //   } catch (e) {
  //     let tab = [];
  //     tab.push(e);
  //     setErrs(tab);
  //     return false;
  //   }
  // }

  const createOrder = async () => {
        try {
          const order = await axios.post(props.url+`/orders`,{
            id_client: cookie.payload.id,
            address: address.value
          });
          if (order.status === 200){
              const o=order.data[0];

            cookie.bagLines.forEach(async (l) => {
              const orderline = await axios.post(props.url+`/order-lines`,{
                order_ref: o.ref,
                product_ref: l.product_ref,
                quantity: l.quantity
              });
              if(orderline.status!== 200){
                return false;
              }
            });
          }
        }
        catch (e) {
          let tab = [];
          tab.push(e);
          setErrs(tab);
          return false
        }
        cookie.bagLines=[];
        setRedirect(true);
        return true;
  }


  return (
    <div>
      {
        cookie.payload && cookie.payload.role==="client" ?
        <div className="container mt-5 pt-5 text-center">
            <h1>Votre panier</h1>

              <div className="modal fade" id="errModal" tabIndex="-1" role="dialog" aria-labelledby="errModalLabel" aria-hidden="true">
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title text-danger" id="errModalLabel"> Erreur(s) </h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div className="modal-body">
                      {
                        errs.length > 0 ?
                        <ul className="list-group list-group-flush">
                          {
                            errs.map(e =>
                              <li className="list-group-item" key={e}>{e}</li>
                            )
                          }
                        </ul>
                        :
                        <div></div>
                      }
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-outline-primary" data-dismiss="modal">Fermer</button>
                  </div>
                </div>
              </div>
              </div>

            <div className="card">
              <div className="card-header">

                  <div>
                    <p className="lead">
                    <span className="col-3 text-muted small">Prénom : </span>
                    {client.first_name}
                    <span className="col-3 text-muted small"> Nom : </span>
                    {client.last_name}
                    <span className="col-3 text-muted small">Email : </span>
                    {client.email}
                  </p>
                  </div>
              </div>
              <div className="card-body">
                <table className="table table-striped table-hover table-responsive-md">
                    <thead className="bg-secondary">
                        <tr>
                            <th scope="col">Ref produit</th>
                            <th scope="col">Produit</th>
                            <th scope="col">Quantité</th>
                            <th scope="col">Prix</th>
                            <th scope="col">Prix TTC</th>
                            <th scope="col">Supprimer</th>
                        </tr>
                    </thead>
                    <tbody>
                      {
                        cookie.bagLines.map(bagLine =>
                              <OrderLine key={bagLine.product_ref} ol={bagLine} refresh={refresh} setRefresh={setRefresh} refreshTotalPrice={refreshTotalPrice} url={props.url} handleDelete={deleteLine}/>)
                      }
                    </tbody>
                  </table>
                  <p>Prix total: {totalPrice.value}</p>
                </div>
                <div className="container">
                    <div>
                          <div className="form-group">
                          <label>Addresse de Livraison</label>
                          <input  type="text" className={address.isValid ? "form-control is-valid" : "form-control is-invalid"} placeholder={address.oldValue ? address.oldValue : "Adresse"} onChange={
                            e => {
                              if(e.target.value!==""){
                                address.value = e.target.value;
                              }
                              else{
                                address.value = address.oldValue;
                              }

                              if (refresh) {
                                setRefresh(false);
                              }
                              else {
                                setRefresh(true);
                              }

                              address.isValid = address.value!==null && address.value!=="";
                            }
                          } />
                      </div>
                    </div>
                </div>

                <div>
                  {
                    (address.isValid && cookie.bagLines.length) >0?
                    <button className="btn btn-info btn-lg mb-3" onClick={() => createOrder()}>Passer commande</button>
                    :
                    <span className="btn btn-info disabled btn-lg mb-3" >Passer commande</span>
                  }
                </div>
            </div>
        </div>
        :
        <Redirect to={"/"}/>
      }
      {redirect && <Redirect to={`/clients/${cookie.payload.id}/orders`}/>}
    </div>
  );
}

export default BagOrder;
