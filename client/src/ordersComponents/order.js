import React, {useEffect, useState} from "react";
import axios from "axios";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAngleDown} from '@fortawesome/free-solid-svg-icons'


const OrderLine = (props) => {
  const {ol} = props;
  const [productName,setProductName] = useState();

  useEffect(
    () => {
      (async function getProduct() {
        const res = await axios.get(props.url+`/products/${ol.product_ref}`);
        if(res.status===200) {
          setProductName(res.data[0].name);
        }
      })();
    },[ol,props.url]
  );

  return(
      <tr>
        <td>{ol.product_ref}</td>
        <td><Link className="btn btn-link" to={`/products/${ol.product_ref}`}>{productName}</Link></td>
        <td>{ol.quantity}</td>
        <td>{ol.price}</td>
        <td>{ol.price_with_vat}</td>
      </tr>

  );
}

const Order = (props) => {
  const [order] = [props.o];
  const [orderLines,setOrderLines] = useState([]);
  const{cookie,config}=props;
  const [status,setStatus] = useState([]);
  const [newStatus] = useState({status:null});

  useEffect(() => {
      (async function getOrderLines() {
          const res = await axios.get(props.url+`/order-lines/${order.ref}`)
          if (res.status === 200){
              setOrderLines(res.data);
          }
      })();

  },[order,props.url]);

  useEffect(() => {
      if(cookie.payload && cookie.payload.role === 'seller'){
        let mounted = true;
          (async function getStatus() {
              const res = await axios.get(props.url+`/status`,config)
              if ((res.status === 200) && mounted){
                  setStatus(res.data);
              }
          })();
          return () => mounted = false;
      }

  },[props,config,cookie]);

  const changeStatus = async () => {
    if(newStatus.status!=null){
      const res = await axios.put(props.url+`/orders/${order.ref}`,newStatus,config)
      if (res.status === 200){
      }
    }
  }

  return (
    <div className="card ">
      <button className="card-header btn btn-block btn-light text-left" id={"heading"+order.ref} data-toggle="collapse" data-target={"#collapse"+order.ref} aria-expanded="true" aria-controls={"collapse"+order.ref}>
          <div className="container"  >
            {
              [order].map(
                order =>
                <div key={order.ref}>
                    <div className="text-center">
                    <div>
                    <FontAwesomeIcon className="float-left " aria-hidden="true" icon={faAngleDown}/>
                      <span className="col-3">Commande #{order.ref}</span>
                      <span className="col-3">Client #{order.id_client}</span>
                      <span className="col-3">Prix total : {order.total_price} Euros</span>
                      <FontAwesomeIcon className="float-right" aria-hidden="true" icon={faAngleDown}/>
                    </div>
                    <br/>
                    <div className="d-sm-block">
                      <span className="col-3">Adresse : {order.address}</span>
                      {
                        order.status==="annulé" ?
                        <span className="col-3 btn btn-outline-secondary mb-1 ml-1">Status : {order.status}</span>
                        :
                        <span>
                        {
                          order.status==="passé" ?
                          <span className="col-3 btn btn-outline-warning mb-1 ml-1">Status : {order.status}</span>
                          :
                          <span>
                          {
                            order.status==="en préparation" ?
                            <span className="col-3 btn btn-outline-primary mb-1 ml-1">Status : {order.status}</span>
                            :
                            <span className="col-3 btn btn-outline-success mb-1 ml-1">Status : {order.status}</span>
                          }
                          </span>
                        }
                        </span>
                      }
                      {
                        cookie.payload.role==="seller" ?
                        <div className="d-inline-flex justify-content-center col-12">
                          <div className="form-inline">
                            <label>Status:</label>
                            <select className="form-control" onChange={
                                e => {
                                  if(e.target.status !== "- -"){
                                    newStatus.status=e.target.value;
                                  }
                                  else{
                                    newStatus.status=null;
                                  }
                                }
                              }>
                              <option>- -</option>
                              {
                                status.map(s => <option key={s}>{s}</option>)
                              }
                            </select>
                            <span className="col-3 btn btn-info btn-sm ml-3 mt-1 mb-1" onClick={changeStatus}>Modifier Status</span>
                          </div>
                          </div>
                        :
                        <div></div>
                      }
                    </div>
                  </div>
                </div>
              )
            }
          </div>
      </button>

      <div id={"collapse"+order.ref} className="collapse" aria-labelledby={"collapse"+order.ref} data-parent="#ordersAccordion">
        <div className="card-body">
          <table className="table table-striped table-hover table-responsive-md">
              <thead className="bg-secondary">
                  <tr>
                      <th scope="col">Ref produit</th>
                      <th scope="col">Produit</th>
                      <th scope="col">Quantité</th>
                      <th scope="col">Prix</th>
                      <th scope="col">Prix TTC</th>
                  </tr>
              </thead>
              <tbody>
                {
                  orderLines.map(orderLine =><OrderLine key={orderLine.order_ref+"-"+orderLine.product_ref} ol={orderLine} url={props.url} />)
                }
              </tbody>
            </table>
        </div>
      </div>
    </div>
  );
}

export default Order;
