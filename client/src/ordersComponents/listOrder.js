import React, {useEffect, useState} from "react";
import axios from "axios";
import { Redirect} from "react-router-dom";


import Order from "./order";

const ListOrders = (props) => {
  const [params,setParams] = useState({offset:0,limit:30});
  const [loading,setLoading] = useState(true);
  const [orders,setOrders] = useState([]);
  const {cookie,config} = props;

  useEffect(
    () => {
      (async function getOrders() {
        const res = await axios.get(props.url+`/orders/limit=${params.limit}-offset=${params.offset}`);
        if(res.status === 200) {
          setLoading(false);
          setOrders(res.data);
        }
      })();
    },[props.url,params]
  );

  const showMore = () => {
    setLoading(true);
    setParams({offset:0,limit:params.limit+30});
    };

  return (
    <div className="container">
      {
        cookie.payload.role === 'seller' ?
        <div className="container mt-5 mb-5 pt-5">
          {
            props.match.params.search  ?
            <h1 className="text-center mt-5 pt-3">Résultat pour "{props.match.params.search}"</h1>
            :
            <h1 className="text-center mt-5 pt-3">Liste des commandes</h1>
          }

          {
            loading ?
            <div className="container mt-2 text-center">
              <h1>Chargement...</h1>
              <div className="spinner-border text-primary" role="status">
                <span className="sr-only">Loading...</span>
              </div>
            </div>
            :
            <div className="container">

              {
                !props.match.params.search || (props.match.params.search && orders.filter(
                  o =>
                    String(o.ref).startsWith(props.match.params.search)
                )
              .length > 0) ?
                <div>
                  <div className="container">
                      {
                        props.match.params.search ?
                        <div className="accordion" id="ordersAccordion">
                          {orders.filter(
                            o =>
                              String(o.ref).startsWith(props.match.params.search))
                              .map(order =>
                              <Order key={order.ref} o={order} url={props.url} cookie={cookie} config={config}/>
                            )
                          }
                        </div>
                        :
                        <div className="accordion" id="ordersAccordion">
                          {orders.map(order =>
                              <Order key={order.ref} o={order} url={props.url} cookie={cookie} config={config}/>
                          )}

                        </div>
                      }
                  </div>
                  <div className="container mt-2 text-center">
                    {
                      orders.length === params.limit ?
                      <button className="btn btn-success btn-md" onClick={showMore}>
                        Voir plus de commandes
                      </button>
                      :
                      <small className="text-secondary text-center">
                        Fin des commandes
                      </small>
                    }
                  </div>
                </div>
                :
                <div>
                  {
                    props.match.params.search  ?
                    <h1 className="text-center mt-5 pt-3">Aucun résultat pour votre recherche</h1>
                    :
                    <h1 className="text-center mt-5 pt-3">Aucunes commandes</h1>
                  }
                </div>
              }
            </div>
          }
          </div>
        :
        <Redirect to={"/"}/>
      }
    </div>
  );

}

export default ListOrders;
