import React, {useEffect, useState} from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAngleRight} from '@fortawesome/free-solid-svg-icons';


const Home = (props) => {
  const {cookie} = props;
  const [products, setProducts] = useState([]);
  const [bestProducts, setBestProducts] = useState([]);

  useEffect(() => {
      (async function getProducts() {
          const res = await axios.get(props.url+`/products/best-sales/limit=20-offset=`);
          const items=[];
          const bestItems=[];
          if (res.status === 200) {
            var i=0;
            var exists = (p) => p.ref === item.ref;
            while (i<4) {
              var item = res.data[Math.floor(Math.random() * res.data.length)];
              var bestItem = res.data[i];

              if(!items.some(exists)){
                items.push(item);
                bestItems.push(bestItem);
                i++;
              }
            }
            setProducts(items);
            setBestProducts(bestItems);
          }
      })();
  },[props.url]);


    return(
      <div className="container mt-5 pt-5">
      <h1 className="col-12 mt-3">Bienvenue sur notre site !</h1>

      <h2 className="col-12">Nos meilleures ventes</h2>

      <div className="row row-cols-sm-1 row-cols-md-2 row-cols-lg-3 ">
        {
          bestProducts.map(
            p =>
            <div key={p.ref} className=" card shadow m-4 justify-content-center">
              <div className="card-header text-center">
              <h5>{p.name}
              </h5>
              {
                p.stock < p.stock_alert && p.stock > 0 ?
                  <span className="badge badge-warning">Bientot épuisé</span>
                :
                <div>
                </div>
              }

              {
                p.stock === 0 ?
                  <span className="badge badge-danger">En rupture de stock</span>
                :
                <div>
                </div>
              }
            </div>

              <img src={props.url+"/"+p.url_img} className="card-img-top" alt="..."/>
              <div className="card-body">

                <p className="card-text">{p.description}</p>
                <Link to={`/products/${p.ref}`} className="btn btn-primary ">Voir le produit <FontAwesomeIcon className="ml-1" aria-hidden="true" icon={faAngleRight}/></Link>
              </div>
            </div>
          )
        }
      </div>

      <h2 className="col-12">À Découvrir</h2>
    <div className="container">
    <div className="row row-cols-sm-1 row-cols-md-2 row-cols-lg-3 ">
      {
        products.map(
          p =>
          <div key={p.ref} className=" card shadow m-4 justify-content-center">
            <div className="card-header text-center">
            <h5>{p.name}
            </h5>
            {
              p.stock < p.stock_alert && p.stock > 0 ?
                <span className="badge badge-warning">Bientot épuisé</span>
              :
              <div>
              </div>
            }

            {
              p.stock === 0 ?
                <span className="badge badge-danger">En rupture de stock</span>
              :
              <div>
              </div>
            }
          </div>
            <img src={props.url+"/"+p.url_img} className="card-img-top" alt="..."/>
            <div className="card-body">

              <p className="card-text">{p.description}</p>
              <Link to={`/products/${p.ref}`} className="btn btn-primary ">Voir le produit</Link>
            </div>
          </div>
        )
      }
    </div>
    </div>

      <div className="container row">
      <Link className="col-lg-4 col-md-8 btn btn-primary btn-lg mr-2 mb-3" to={"/products"}>Voir tout nos produits</Link>
      {
        cookie.payload && cookie.payload.role==="client" ?
        <Link className="col-lg-4 col-md-8 btn btn-success btn-lg mr-2 mb-3 " to={`/clients/${cookie.payload.id}/orders`}>Vos Commandes</Link>
        :
        <div></div>
      }
      {
        !cookie.payload ?
        <Link className="col-lg-4 col-md-8 btn btn-success btn-lg mr-2 mb-3" to={"/login"}>Connectez-vous</Link>
        :
        <div></div>
      }
      </div>
    </div>
    );
}

export default Home;
