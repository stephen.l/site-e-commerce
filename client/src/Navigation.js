import React,{useState,useEffect} from "react";
import {Link,Redirect} from "react-router-dom";
import $ from 'jquery';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSearch,faShoppingBasket} from '@fortawesome/free-solid-svg-icons';

const Navigation = (props) => {
    const {cookie, removeCookie, setExpToken} = props;
    const [search,setSearch] = useState(null);
    const [redirect,setRedirect]= useState(false);
    const [refresh,setRefresh]= useState(false);

    const logout = () => {
        removeCookie('payload',{sameSite:false});
        removeCookie('token',{sameSite:false});
        setExpToken(0);
        if (refresh) {
          setRefresh(false);
        }
        else {
          setRefresh(true);
        }
    }

    const searchSubmit = async e => {
        e.preventDefault();
        if(search!==null && search!==""){
          await setRedirect(true);
          setRedirect(false);

        }

    }

    useEffect(
      () => {
      },[cookie,refresh]
    );

    // Permet de refermer le menu de nav en mobile au clic sur un lien

    $(function(){
        var navMain = $(".navbar-collapse");
        navMain.on("click", "a:not([data-toggle])", null, function () {
            navMain.collapse('hide');
        });
    });

    return (
      <nav className="navbar navbar-expand-xl navbar-light bg-light fixed-top mb-5 shadow" >

        <div className="lead nav-item row mx-2">
          <span className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </span>
          <Link to="/">
              <span className="nav-link text-dark">
                LMLS Shopping
              </span>
            </Link>
        </div>

        <div className="collapse navbar-collapse" id="navbarCollapse">
          <ul className="navbar-nav mr-auto">

            {(cookie.payload && (cookie.payload.role === 'admin')) ?
              <li className="nav-item">
                  <Link to="/clients">
                      <span className="nav-link">Liste Clients</span>
                  </Link>

              </li>:
              <li>
              </li>
            }
            <li className="nav-item">
              <Link to="/products">
                  <span className="nav-link">Produits</span>
              </Link>
            </li>
            {(cookie.payload && (cookie.payload.role === 'seller')) ?
                <li className="nav-item">
                  <Link to="/products/create">
                      <span className="nav-link">Créer un Produit</span>
                  </Link>
                </li>:
                <li>
                </li>
            }
            {(cookie.payload && (cookie.payload.role === 'seller')) ?
              <li className="nav-item">
                <Link to="/orders">
                    <span className="nav-link">Commandes</span>
                </Link>
              </li>
              :
              <li></li>
            }
            {(cookie.payload && (cookie.payload.role === 'client')) ?
              <li className="nav-item">
                <Link className="outline-0"to={`/clients/${cookie.payload.id}/orders`}>
                    <span className="nav-link">Mes Commandes</span>
                </Link>
              </li>
              :
              <li></li>
            }


              {!cookie.payload ?
                <div className="d-block d-xl-none mb-2">
                  <li className="nav-item">
                    <Link to="/register">
                        <span className="btn btn-secondary rounded-pill mb-2" >S'inscrire</span>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/login">
                        <span className="btn btn-primary rounded-pill">Se connecter</span>
                    </Link>
                  </li>
                </div>
                  :
                  <div className="d-block d-xl-none mb-2">
                    {
                      cookie.payload.role==="client" ?
                      <li className="nav-item">
                        <Link to="/bag">
                            <span className="btn btn-info mb-2 rounded-pill" ><FontAwesomeIcon className="mr-1" icon={faShoppingBasket}/>Mon Panier</span>
                        </Link>
                      </li>
                      :
                      <div></div>
                    }
                    <li className="nav-item">
                      <Link to="/me">
                          <span className="btn btn-outline-success rounded-pill mb-2">Mon Profil</span>
                      </Link>
                    </li>
                    <li className="nav-item">
                      <Link to ="/">
                        <button className="btn btn-outline-danger rounded-pill" onClick={logout}>
                            <span>Se déconnecter</span>
                        </button>
                      </Link>
                    </li>
                  </div>
              }

          <div className="navbar-nav d-block d-xl-none">
            <form className="form-inline" onSubmit={searchSubmit}>
              <input className="form-control rounded-pill" type="search" placeholder="Rechercher" aria-label="Rechercher" onChange={
                  e =>
                  {
                    setSearch(e.target.value);
                  }
                }/>
              <button className="btn btn-outline-primary my-2 my-sm-0 rounded-pill" type="submit"><FontAwesomeIcon title="Rechercher" icon={faSearch}/></button>
            </form>
          </div>

            <div className="navbar-nav d-none d-xl-inline navbar-center">
              <form className="form-inline" onSubmit={searchSubmit}>
                <input className="form-control mr-sm-2 rounded-pill" type="search" placeholder="Rechercher" aria-label="Rechercher" onChange={
                    e =>
                    {
                      setSearch(e.target.value);
                    }
                  }/>
                <button className="btn btn-outline-primary my-2 my-sm-0 rounded-pill" type="submit"><FontAwesomeIcon title="Rechercher" icon={faSearch}/> </button>
              </form>
            </div>
          </ul>
            <div className="d-none d-xl-inline navbar-nav ml-md-auto">

              {!cookie.payload ?
                <div className="container">
                  <li className="nav-item">
                    <Link to="/register">
                        <span className="btn btn-secondary mr-2 rounded-pill" >S'inscrire</span>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/login">
                        <span className="btn btn-primary rounded-pill">Se connecter</span>
                    </Link>
                  </li>
                </div>
                  :
                  <div className="container">
                    {
                      cookie.payload.role==="client" ?
                      <li className="nav-item">
                        <Link to="/bag">
                    <span className="btn btn-info mr-2 rounded-pill" >
                      <FontAwesomeIcon className="mr-1" icon={faShoppingBasket}/>
                      Mon Panier
                      {
                        cookie.bagLines.length > 0 ?
                        <span class="badge badge-light">{cookie.bagLines.length}</span>
                        :
                        <span></span>
                      }
                      </span>
                        </Link>
                      </li>
                      :
                      <div></div>
                    }
                    <li className="nav-item">
                      <Link to="/me">
                          <span className="btn btn-outline-success mr-2 rounded-pill">Mon Profil</span>
                      </Link>
                    </li>
                    <li className="nav-item">
                      <Link to={'/'}>
                        <button className="btn btn-outline-danger rounded-pill" onClick={logout}>
                            <span>Se déconnecter</span>
                        </button>
                      </Link>
                    </li>
                  </div>
              }
          </div>
        </div>

        {
          cookie.payload && (cookie.payload.role==="seller" || cookie.payload.role==="admin") ?
          <div>
            {
              cookie.payload.role==="seller" ?
              <div>
                {redirect && (<Redirect to={`/orders/search=${search}`}/>)}
              </div>
              :
              <div>
                {redirect && (<Redirect to={`/clients/search=${search}`}/>)}
              </div>
            }
          </div>
          :
          <div>{redirect && (<Redirect to={`/products/search=${search}`}/>)}</div>
        }
      </nav>
    );
}

export default Navigation;
