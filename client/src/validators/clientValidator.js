import validator from "validator";

// On fais un validateur par champs, chaque champ du composant sera lié à son validateur
// Le bouton envoyer est lié à un validateur globale

// We make a validator per field, each field of the component will use the validator
// The submit button will use a global validator

const validateName = (first_name) => {
  return !validator.isEmpty(first_name,{ ignore_whitespace:false })
}

const validateMail = (mail) => {
  return (
    !validator.isEmpty(mail,{ ignore_whitespace:false }) &&
    validator.isEmail(mail)
  );
}

const validatePassword = (password) => {
  return (
    !validator.isEmpty(password,{ ignore_whitespace:false }) &&
    validator.isLength(password,{ min: 6}) &&
    validator.matches(password,/\d+/) &&
    validator.matches(password,/[a-z]/) &&
    validator.matches(password,/[A-Z]/) &&
    validator.matches(password,/[^a-zA-Z0-9]/)
  );
}


const validateClient = (newClient) =>{
    try {
      return (
          validateName(newClient.first_name) &&
          validateName(newClient.last_name) &&
          validateMail(newClient.email) &&
          validatePassword(newClient.password)
        );

    } catch (e) {
      return false;
    }
}

const validateUpdate = (newClient) => {
  try {
    return (
        validateName(newClient.first_name) &&
        validateName(newClient.last_name) &&
        validateMail(newClient.email) &&
        !validator.isEmpty(newClient.oldPassword,{ ignore_whitespace:false })
      );

  } catch (e) {
    return false;
  }
}

export {
  validateName,
  validateMail,
  validatePassword,
  validateClient,
  validateUpdate
};
