import validator from "validator";

const validateName = (name) => {
  return !validator.isEmpty(name,{ ignore_whitespace:false })
}

const validateInt = (n) => {
  return (
    !validator.isEmpty(n,{ ignore_whitespace:false }) &&
    validator.isInt(n) &&
    n>0
  );
}

const validatePrice = (price) => {
  return (
    !validator.isEmpty(price,{ ignore_whitespace:false }) &&
    price>0
  );
}

const validateImg = (img) => {
  return (
    !validator.isEmpty(img,{ ignore_whitespace:false })
  );
}

const validateProduct = (newProduct) =>{
    try {
      return (
          validateName(newProduct.name) &&
          validateInt(newProduct.stock) &&
          validateInt(newProduct.stock_alert) &&
          validatePrice(newProduct.price) &&
          validateImg(newProduct.file.name)
        );

    } catch (e) {
      return false;
    }
}

export {
  validateName,
  validateInt,
  validatePrice,
  validateProduct,
  validateImg
};
