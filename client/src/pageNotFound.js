import React from "react";
import {Link} from "react-router-dom";

const PageNotFound = (props) => {
  return (
    <div className="container pt-5 mt-5 text-center">
      <h1 className="display-1 mt-3">Erreur 404</h1>
      <h2 className="display-2">Page non trouvée</h2>
      <Link to={"/"} className="btn btn-secondary btn-lg">Retour à l'accueil</Link>
    </div>
  );
}

export default PageNotFound;
