import React from "react";
import {Redirect} from "react-router-dom";

import ListClientOrders from "./listClientOrders";

const ClientOrders = (props) => {
  const {cookie,config} = props;


  return (
    <div className="container">
      {
        !(cookie.payload && (Number(cookie.payload.id) === Number(props.match.params.id) )) ?
        <Redirect to={"/"}/> :
        <ListClientOrders id={props.match.params.id} cookie={cookie} config={config} url={props.url}/>
      }
    </div>
  );
}

export default ClientOrders;
