import React, {useState,useEffect} from "react";
import axios from 'axios';

const AdminForm = (props) => {
  const {client, handleSubmit,config,cookie} = props;
  const [roles,setRoles] = useState([]);
  const [oldPasswordConfirmed,setOldPasswordConfirm] = useState(false);

  useEffect(() => {
      if(cookie.payload && cookie.payload.role === 'admin'){
        let mounted = true;
          (async function getRoles() {
              const res = await axios.get(props.url+`/roles`,config)
              if ((res.status === 200) && mounted){
                  setRoles(res.data);
              }
          })();
          return () => mounted = false;
      }

  },[props,config,cookie]);


  return(
      <div className="container">
          <form onSubmit={handleSubmit}>

            <div className="form-group">
              <label>Sélectionner un nouveau role :</label>
              <select className="form-control" onChange={
                  e => {
                    client.role = e.target.value;
                  }
                }>
                {
                  roles.map(r => <option key={r}>{r}</option>)
                }
              </select>
            </div>

            <div className="form-group">
              <label>
                Confirmer les changements avec votre mot de passe.
              </label>
              <input type="password" className={oldPasswordConfirmed ? "form-control is-valid":"form-control is-invalid"} placeholder="Confirmer mot de passe" onChange={e =>
                  {
                    client.oldPassword = e.target.value;
                    setOldPasswordConfirm(client.oldPassword!=null)
                  }
                }
              />
              {
                !oldPasswordConfirmed ?
                <div>
                  <small className="form-text text-danger m-1 mb-2">
                    Le mot de passe est requis
                  </small>
              </div>
                :
                <div></div>
              }
            </div>

            {
              oldPasswordConfirmed ?
              <button type="submit" className="btn btn-primary btn-md">
                Valider
              </button>
              :
              <span type="submit" className="btn btn-primary disabled btn-md">
                Valider
              </span>
          }

          </form>
      </div>
  )
}

export default AdminForm;
