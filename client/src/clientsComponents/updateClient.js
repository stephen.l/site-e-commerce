import React, {useEffect, useState} from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
import $ from 'jquery';

import ClientForm from "./clientForm";
import AdminForm from './adminForm';
import {validateUpdate} from '../validators/clientValidator';

const UpdateClient = (props) => {
  const [updateClient,setUpdateClient] = useState({last_name: null,first_name: null,email: null,password: null,role: null,address: null,oldPassword:null});

  const [redirect, setRedirect] = useState(false);
  const {config, cookie} = props;
  const [errs,setErrs] = useState([]);

    useEffect(() => {
        if((cookie.payload && ((cookie.payload.role === 'admin') || (Number(cookie.payload.id) === Number(props.match.params.id))))){
            (async function modifClient() {
                const res = await axios.get(props.url+`/clients/${props.match.params.id}`,config)
                if (res.status === 200){
                    setUpdateClient(res.data[0]);
                }
            })();
        }

    },[props,config,cookie]);

    useEffect(() => {
        if(errs.length>0){
          $('#errModal').modal('toggle');
        }
    }, [errs]);


    const handleSubmit = e => {
        e.preventDefault();
        (async function modifClient() {
          setErrs([]);
            try {
              const res = await axios.put(props.url+`/clients/${props.match.params.id}`,updateClient,config);
              console.log(res.status);
              if (res.status === 200){
                  await setRedirect(true);
              }
            } catch (e) {
              let tab =[];
              e.response.data.errors.forEach(er => tab.push(Object.values(er)));
              setErrs(tab);

            }
        })();
    }

    const validate = (client) => {
      return validateUpdate(client);
    }

    return(
        <div className="container mt-5 pt-5">
            {!(cookie.payload && ((cookie.payload.role === 'admin') || (Number(cookie.payload.id) === Number(props.match.params.id)))) ? <Redirect to={"/"}/> :
                <div>
                    <h1 className="text-center">Modification du compte</h1>

                      <div className="modal fade" id="errModal" tabIndex="-1" role="dialog" aria-labelledby="errModalLabel" data-backdrop="static" aria-hidden="true">
                      <div className="modal-dialog">
                        <div className="modal-content">
                          <div className="modal-header">
                            <h5 className="modal-title text-danger" id="errModalLabel"> Erreur(s) </h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div className="modal-body">
                              {
                                errs.length > 0 ?
                                <ul className="list-group list-group-flush">
                                  {
                                    errs.map(e =>
                                      <li className="list-group-item" key={e}>{e}</li>
                                    )
                                  }
                                </ul>
                                :
                                <div></div>
                              }
                          </div>
                          <div className="modal-footer">
                            <button type="button" className="btn btn-outline-primary" data-dismiss="modal">Fermer</button>
                          </div>
                        </div>
                      </div>
                      </div>

                    {
                      Number(cookie.payload.id)!==Number(props.match.params.id) && cookie.payload.role === 'admin' ?
                      <AdminForm
                        client={updateClient}
                        handleSubmit={handleSubmit}
                        url={props.url}
                        config={config}
                        cookie={cookie}
                      />
                      :
                      <ClientForm
                        client={updateClient}
                        handleSubmit={handleSubmit}
                        validate={validate}
                        isUpdate={true}
                      />
                    }

                  {
                    cookie.payload.role === "admin" ?
                    redirect && (<Redirect to={`/clients`}/>)
                    :
                    redirect && (<Redirect to={`/me`}/>)
                  }
        </div>
      }
    </div>
    )
}

export default UpdateClient;
