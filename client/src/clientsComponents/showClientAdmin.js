import React from "react";
import {Redirect} from "react-dom";
import ShowClient from "./showClient";


const ShowClientAdmin = (props) => {
    const {config, cookie} = props;

        return (
          <div>
            {
              (cookie.payload.role === 'admin') ?
              <ShowClient id={props.match.params.id} config={config} cookie={cookie} url={props.url}/>
              :
              <Redirect to={"/"}/>
            }
          </div>
        );
};

export default ShowClientAdmin;
