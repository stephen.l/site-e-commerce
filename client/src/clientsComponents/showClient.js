import React, {useEffect, useState} from "react";
import {Link, Redirect} from "react-router-dom";
import axios from "axios";


import ListClientOrders from "./listClientOrders";

const ShowClient = (props) => {
    const [client, setClient] = useState([]);
    const [redirect, setRedirect] = useState(false);
    const {config, cookie} = props;

    useEffect(() => {
        if(cookie.payload) {
            (async function getClient() {
                const res = await axios.get(props.url+`/clients/${props.id}`, config);
                if (res.status === 200){
                    setClient(res.data);
                }
            })();
        }
    },[props,config,redirect,cookie]);

    const handleDelete = (id) => {
        (async function deleteClient() {
            const res = await axios.delete(props.url+`/clients/${id}`, config);
            if (res.status === 200) {
                setRedirect(true);
            }
        })();
    }

        return (
            <div className="container mt-5 pt-5">
                {!cookie.payload ? <Redirect to={"/"}/> :

                <div className="container">
                  {
                    cookie.payload.role === "admin" ?
                    <h1>Profil du client</h1>
                    :
                    <h1>Mon profil</h1>
                  }

                <div className="container mb-5 col-12">
                  {client.map(client =>
                    <div className="row" key={client.id}>

                      <div className="col-6">
                          <p className="lead">
                            <span className="text-muted small">Prénom : </span>
                            {client.first_name}
                            <br/>
                            <span className="text-muted small"> Nom : </span>
                            {client.last_name}
                            <br/>
                            <span className="text-muted small">Email : </span>
                            {client.email}
                            <br/>
                            <span className="text-muted small">Addresse : </span>
                            {client.address}
                            <br/>
                            {
                              cookie.payload.role === "admin" ?
                                <span>
                                  <span className="text-muted small">Role : </span>{client.role}
                                </span>
                                :
                                <span></span>
                            }
                          </p>
                      </div>

                      {(cookie.payload.id === client.id || (cookie.payload.role === 'admin')) ?
                          <div className="col-6">
                          <Link to={`/clients/update/${client.id}`}>
                              <span className="btn btn-warning mb-2">Modifier</span>
                          </Link>
                          <br/>
                          <button className="btn btn-danger" onClick={() => handleDelete(client.id)}>Supprimer</button>
                          </div> :
                          <div>
                          </div>
                      }
                      </div>
                    )}
                  </div>

                  <div className="content mt-5">
                      {client.map(client =>
                          <ListClientOrders key={client.id} id={client.id} config={config} cookie={cookie} url={props.url}/>)}

                  </div>
                    {redirect && (<Redirect to={"/"}/>)}
                </div>
                }
            </div>
        );
};

export default ShowClient;
