import React, {useState,useEffect} from "react";

import {validateName,validateMail,validatePassword} from '../validators/clientValidator';

const ClientForm = (props) => {
    const {client, handleSubmit, isUpdate, validate} = props;
    const [oldValues,setOldValues] = useState(
      {
        last_name: client.last_name,
        first_name: client.first_name,
        email: client.email,
        password: client.password,
        role: client.role,
        address: client.address,
        oldPassword: client.oldPassword
      }
    );

    useEffect(() => {
      setOldValues(
        {
          last_name: client.last_name,
          first_name: client.first_name,
          email: client.email,
          password: client.password,
          role: client.role,
          address: client.address,
          oldPassword: client.oldPassword
        }
      )
    },[client]);


    const [lastNameIsValid,setLastNameIsValid] = useState(isUpdate ? true : false);
    const [firstNameIsValid,setFirstNameIsValid] = useState(isUpdate ? true : false);
    const [MailIsValid,setMailIsValid] = useState(isUpdate ? true : false);
    const [passwdIsValid,setPasswdIsValid] = useState(isUpdate ? true : false);
    const [confirmPasswd] = useState({value: ''});
    const [confirmed,setIsConfirmed] = useState(true);
    const [oldPasswordConfirmed,setOldPasswordConfirm] = useState(false);
    const [isValid,setIsValid] = useState(false);

    return(
        <div className="container">
            <form onSubmit={handleSubmit}>

                <div className="form-group">
                  <label>Nom</label>
                  <input type="text" className={lastNameIsValid ? "form-control is-valid":"form-control is-invalid"} placeholder={oldValues.last_name ? oldValues.last_name : "Nom"} onChange={e =>
                      {
                        client.last_name = e.target.value;
                        if(isUpdate && e.target.value===""){
                          client.last_name = oldValues.last_name;
                        }

                      setLastNameIsValid(validateName(client.last_name));
                      setIsValid(validate(client));
                      }
                    }
                  />
                {
                  !lastNameIsValid ?
                  <small className="form-text text-danger m-1 mb-2">Nom requis</small>
                  :
                  <div></div>
                }
                </div>

                <div className="form-group">
                  <label>Prénom</label>
                  <input type="text" className={firstNameIsValid ? "form-control is-valid":"form-control is-invalid"} placeholder={oldValues.first_name ? oldValues.first_name : "Prénom"} onChange={e =>
                      {
                          client.first_name = e.target.value;
                          if(isUpdate && e.target.value===""){
                            client.first_name = oldValues.first_name;
                          }
                        setFirstNameIsValid(validateName(client.first_name));
                        setIsValid(validate(client));
                      }
                    }
                  />
                  {
                    !firstNameIsValid ?
                    <small className="form-text text-danger m-1 mb-2">Prénom requis</small>
                    :
                    <div></div>
                  }
                </div>

                <div className="form-group">
                  <label>Email</label>
                  <input type="text" className={MailIsValid ? "form-control is-valid":"form-control is-invalid"} placeholder={oldValues.email ? oldValues.email : "exemple@mail.com"} onChange={e =>
                      {
                          client.email = e.target.value;
                          if(isUpdate && e.target.value===""){
                            client.email = oldValues.email;
                          }
                        setMailIsValid(validateMail(client.email));
                        setIsValid(validate(client));
                      }
                    }
                  />
                  {
                    !MailIsValid ?
                    <small className="form-text text-danger m-1 mb-2">Mail non valide</small>
                    :
                    <div></div>
                  }
                </div>

                <div className="form-group">
                    {
                      isUpdate ?
                        <label>
                          Nouveau mot de passe
                        </label>
                        :
                        <label>
                          Mot de passe
                        </label>
                    }
                  <input type="password" className={passwdIsValid ? "form-control is-valid":"form-control is-invalid"} placeholder="Mot de passe" onChange={e =>
                      {
                          client.password = e.target.value;
                          if(isUpdate && e.target.value===""){
                            client.password = oldValues.password;
                            setPasswdIsValid(true);
                            setIsConfirmed(!confirmPasswd);
                          }
                          else{
                            setPasswdIsValid(validatePassword(client.password));

                            setIsConfirmed(confirmPasswd.value===client.password);
                          }
                        setIsValid(validate(client));
                      }
                    }
                  />
                  {
                    !passwdIsValid ?
                    <div><small className="form-text text-danger m-1 mb-2">Mot de passe non valide, il doit contenir : </small>

                    <ul>
                      <li>
                        <small className="form-text text-secondary m-1 mb-2">Au moins 6 caractères</small>
                      </li>
                      <li>
                        <small className="form-text text-secondary m-1 mb-2">Au moins 1 majuscule</small>
                      </li>
                      <li>
                        <small className="form-text text-secondary m-1 mb-2">Au moins 1 minuscule</small>
                      </li>
                      <li>
                        <small className="form-text text-secondary m-1 mb-2">Au moins 1 chiffre</small>
                      </li>
                      <li>
                        <small className="form-text text-secondary m-1 mb-2">Au moins 1 caractères non alpha-numérique</small>
                      </li>
                    </ul></div>
                    :
                    <div></div>
                  }
                </div>

                <div className="form-group">
                  {
                    isUpdate ?
                      <label>
                        Confirmer nouveau mot de passe
                      </label>
                      :
                      <label>
                        Confirmer Mot de passe
                      </label>
                  }
                  <input type="password" className={confirmed ? "form-control is-valid":"form-control is-invalid"} placeholder="Confirmer mot de passe" onChange={e =>
                      {
                        confirmPasswd.value=e.target.value;


                        if(isUpdate && !client.password){
                          setIsConfirmed(true);
                          console.log("problème");
                        }
                        else{
                          setIsConfirmed(confirmPasswd.value===client.password);
                        }
                      setIsValid(validate(client));
                      }
                    }
                  />
                  {
                    !confirmed ?
                    <div>
                      <small className="form-text text-danger m-1 mb-2">
                        Les mots de passe sont différents
                      </small>
                  </div>
                    :
                    <div></div>
                  }
                </div>

                <div className="form-group">
                  <input type="text" className="form-control" placeholder={oldValues.address ? oldValues.address : "Adresse"} onChange={e => client.address = e.target.value} />
                </div>

                {
                  isUpdate ?
                  <div className="form-group">
                    <label>
                      Confirmer les changements avec votre mot de passe actuel
                    </label>
                    <input type="password" className={oldPasswordConfirmed ? "form-control is-valid":"form-control is-invalid"} placeholder="Confirmer mot de passe" onChange={e =>
                        {
                            client.oldPassword = e.target.value;
                            if(isUpdate && e.target.value===""){
                              client.oldPassword = oldValues.oldPassword;
                            }
                          setIsValid(validate(client));
                          setOldPasswordConfirm(client.oldPassword!=null)
                        }
                      }
                    />
                    {
                      !oldPasswordConfirmed ?
                      <div>
                        <small className="form-text text-danger m-1 mb-2">
                          Le mot de passe est requis
                        </small>
                    </div>
                      :
                      <div></div>
                    }
                  </div>
                  :
                  <div></div>
                }


                {
                  isUpdate ?
                  <div className="container">
                    {
                      isValid && oldPasswordConfirmed && confirmed ?
                      <button type="submit" className="btn btn-primary btn-md">
                        Valider
                      </button>
                      :
                      <span className="btn btn-primary btn-md disabled">
                        Valider
                      </span>
                    }
                  </div>
                  :
                  <div className="container">
                    {
                      isValid && confirmed ?
                      <button type="submit" className="btn btn-primary btn-md rounded-pill">
                        S'inscrire
                      </button>
                      :
                      <span className="btn btn-primary btn-md disabled rounded-pill">
                        S'inscrire
                      </span>
                    }
                  </div>
                }

            </form>
        </div>
    )
}

export default ClientForm;
