import React, {useEffect, useState} from "react";
import axios from "axios";
import {Link,Redirect} from "react-router-dom";

const Client = (props) => {
    const [client] = [props.c];

    return(
        <tr>
            <th>{client.id}</th>
            <td>
                <Link to={`/clients/${client.id}`}>
                    <span>{client.first_name} {client.last_name}</span>
                </Link>
            </td>
        </tr>
    )
}

const ListClients = (props) => {
  const [params,setParams] = useState({offset:0,limit:30});
  const [loading,setLoading] = useState(true);
  const [clients, setClients] = useState([]);
  const {config, cookie} = props;

  useEffect(() => {
      if((cookie.payload && (cookie.payload.role === 'admin'))) {
          (async function getClients() {
              const res = await axios.get(props.url+`/clients/limit=${params.limit}-offset=${params.offset}`,config);
              if (res.status === 200) {
                setLoading(false);
                setClients(res.data);
              }
          })();
      }
  },[config, cookie,props.url,params]);

  const showMore = () => {
    setLoading(true);
    setParams({offset:0,limit:params.limit+30});
  };

  return (
    <div>
    {
      !(cookie.payload && (cookie.payload.role === 'admin')) ?
      <Redirect to={"/"}/>
        :

        <div className="container mt-5 mb-5 pt-5 col-8">
          {
            props.match.params.search  ?
            <h1 className="text-center mt-5 pt-3">Résultat pour "{props.match.params.search}"</h1>
            :
            <h1 className="text-center mt-5 pt-3">Liste des clients</h1>
          }
          {
            loading ?
            <div className="container mt-2 text-center">
              <h1>Chargement...</h1>
              <div className="spinner-border text-primary" role="status">
                <span className="sr-only">Loading...</span>
              </div>
            </div>
            :
            <div className="container col-8">
                {
                  !props.match.params.search || (props.match.params.search && clients.filter(
                    c =>
                      [c.last_name,c.first_name].some(w=>w.startsWith(props.match.params.search))
                  )
                .length > 0) ?
                <div>
                  {
                    props.match.params.search ?

                    <table className="table mt-4">
                        <thead className="thead-dark">
                            <tr>
                                <th>Id</th>
                                <th>Nom et Prénom</th>
                            </tr>
                        </thead>
                        <tbody>
                        {clients.filter(
                          c =>
                            [c.last_name,c.first_name].some(w=>w.startsWith(props.match.params.search))
                        ).map(client =>
                            <Client key={client.id} c={client}/>
                        )}
                        </tbody>
                    </table>
                    :
                    <table className="table mt-4">
                        <thead className="thead-dark">
                            <tr>
                                <th>Id</th>
                                <th>Nom et Prénom</th>
                            </tr>
                        </thead>
                        <tbody>
                        {clients.map(client =>
                            <Client key={client.id} c={client}/>
                        )}
                        </tbody>
                    </table>
                  }
                  <div className="container mt-2 text-center">
                    {
                      clients.length === params.limit ?
                      <button className="btn btn-success btn-md" onClick={showMore}>
                        Voir plus de clients
                      </button>
                      :
                      <small className="text-secondary text-center">
                        Fin des clients
                      </small>
                    }
                  </div>
                </div>
                :
                <div>
                  {
                    props.match.params.search  ?
                    <h1 className="text-center mt-5 pt-3">Aucun résultat pour votre recherche</h1>
                    :
                    <h1 className="text-center mt-5 pt-3">Aucune clients</h1>
                  }
                </div>
                }

            </div>
          }
        </div>
      }
      </div>
  );
}

export default ListClients;
