import React, {useEffect, useState} from "react";
import axios from "axios";
import {Link} from "react-router-dom";

import Order from "../ordersComponents/order";

const ListClientOrders = (props) => {
  const {id,config,cookie} = props;
  const [params,setParams] =  useState({offset:0,limit:15});
  const [loading,setLoading] = useState(true);
  const [orders,setOrders] = useState([]);

  useEffect(
    () => {
      (async function getOrders() {
        const res = await axios.get(props.url+`/clients/${id}/orders/limit=${params.limit}-offset=${params.offset}`,config);
        if(res.status === 200) {
          setOrders(res.data);
          setLoading(false);
        }
      })();
    },[props,config,id,params,loading]
  );

  const showMore = () => {
    setLoading(true);
    setParams({offset:0,limit:params.limit+15});
  };

  return (
    <div className="container">
      {
        cookie.payload.role==="client" ?
        <div className="container mb-5">
          {
            loading ?
            <div className="container mt-2 pt-2 text-center">
              <h1>Chargement...</h1>
              <div className="spinner-border text-primary" role="status">
                <span className="sr-only">Loading...</span>
              </div>
            </div>
            :
            <div className="container mt-5 pt-5">
              {
                orders.length>0 ?
                <div className="container">
                  <h1 className="text-center">Vos dernières commandes</h1>

                  <div className="accordion" id="ordersAccordion">
                    {orders.map(order =>
                        <Order key={order.ref} o={order} url={props.url} cookie={cookie}/>
                    )}
                  </div>
                  <div className="container text-center">
                      {
                        orders.length === params.limit ?
                        <button className="btn btn-success btn-md mt-2" onClick={showMore}>
                          Afficher plus de commandes
                        </button>
                        :
                        <small className="text-secondary text-center">
                          Fin des commandes
                        </small>
                      }
                  </div>
                </div>
                :
                <div className="container text-center">
                  <h1>
                    Vous n'avez pas encore commandé chez nous.
                  </h1>
                  <br/>
                  <Link to="/products" className="btn btn-lg btn-primary">
                    Voir nos produits
                  </Link>
                </div>
              }
            </div>
          }
        </div>
        :
        <div></div>
      }
    </div>
  );

}

export default ListClientOrders;
