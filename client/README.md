# Projet e-Commerce

Projet de fin d'étude du DUT Info

Le  but  de  ce  projet  est  de  créer  une  application  de  commerce  en  ligne  à  l’aide  du framework JavaScript [React.js](https://fr.reactjs.org/)  pour  la  partie front-end.

## Tester le server

`npm install`

`npm start`

Le serveur est maintenant disponible sur **localhost:3000**.
