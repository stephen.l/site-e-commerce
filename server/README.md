# Projet e-Commerce

Projet de fin d'étude du DUT Info

Le  but  de  ce  projet  est  de  créer  une  application  de  commerce  en  ligne  à  l’aide  du framework JavaScript [Express.js](http://expressjs.com/) pour la partie back-end.

## Tester le server

`npm install`

Vous devez avoir installer [postgresql](https://www.postgresql.org/) et des variables d'environnement correctes.

`psql -f db/pg.sql`

`npm start`

Le serveur est maintenant disponible sur **localhost:8000** ou sur l'adresse configuré.
