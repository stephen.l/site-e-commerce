const {check} = require('express-validator');
const db = require('../../db/pg');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

// on ne fais qu'un seul validateur pour client car update et create se font sur les mêmes valeurs
// Si ce sont des valeurs différentes on ferais 2 validateur différents dans ce fichier

// We only do one validator because create and update ask for the same field
// If not, we would have done 2 validators

const validateClient = () => {
  return [
    check('last_name').notEmpty().withMessage("Le nom est requis"),
    check('first_name').notEmpty().withMessage("Le prénom est requis"),
    check('password')
      .isLength({ min: 6}).withMessage("Le mot de passe doit contenir au moins 6 caractères")
      .matches(/\d+/).withMessage('Le mot de passe doit contenir au moins un chiffre')
      .matches(/[a-z]/).withMessage('Le mot de passe doit contenir au moins une lettre minuscule')
      .matches(/[A-Z]/).withMessage('Le mot de passe doit contenir au moins une lettre majuscule')
      .matches(/[^a-zA-Z0-9]/).withMessage('Le mot de passe doit contenir au moins un caractère non alplha-numeric')
  ];
}

exports.validateCreate = () => {
  return [
    validateClient(),
    check('email')
      .isEmail().withMessage('Le mail est invalide')
      .custom(emailIsUnique)
  ];
}

exports.validateUpdate = () => {
  return [
    check('last_name').notEmpty().withMessage("Le nom est requis"),
    check('first_name').notEmpty().withMessage("Le prénom est requis"),
    check('password')
      .optional({nullable: true})
      .isLength({ min: 6}).withMessage("Le mot de passe doit contenir au moins 6 caractères")
      .matches(/\d+/).withMessage('Le mot de passe doit contenir au moins un chiffre')
      .matches(/[a-z]/).withMessage('Le mot de passe doit contenir au moins une lettre minuscule')
      .matches(/[A-Z]/).withMessage('Le mot de passe doit contenir au moins une lettre majuscule')
      .matches(/[^a-zA-Z0-9]/).withMessage('Le mot de passe doit contenir au moins un caractère non alplha-numeric'),
    check('email')
      .isEmail().withMessage('Le mail est invalide')
      .custom(updateMail),
    check('oldPassword')
      .custom(matchPassword),
    check('role')
      .notEmpty().withMessage("Le role est requis")
      .custom(checkRoleValue)
  ];
}

const emailIsUnique = async (value) => {
  const result = await db.query({
    text: 'select email::text from clients where email=$1',
    values:[value],
    rowMode: 'array'
  });

  if (result.rows.length!=0) {
    throw 'Le mail est déjà pris';
  }
}

const updateMail = async (value, {req}) => {
  const result = await db.query({
    text: 'select email::text from clients where email=$1 and id!=$2',
    values:[value,req.params.id],
    rowMode: 'array'
  });

  if (result.rows.length!=0) {
    throw 'Le mail est déjà pris';
  }
}

const matchPassword = async (value, {req}) => {
  const tokenArray = req.headers.authorization.split(" ");
  const payload = jwt.decode(tokenArray[1]);
  let val;
  if (payload.role === 'admin'){
    val = payload.id;
  } else {
    val = req.params.id
  }
  const result = await db.query({
    text: 'select * from clients where id=$1',
    values:[val]
  });
  const cmp = await bcrypt.compare(value, result.rows[0].password);

  if(!cmp){
    throw 'Le mot de passe ne correspond pas';
  }
}

const checkRoleValue = async (value) => {
  const result = await db.query({
    text: 'SELECT unnest(enum_range(NULL::role)) as roles',
    rowMode: "array"
  });
  let isValid = false;

  for (r of result.rows) {
    if(String(r)===String(value)){
      isValid = true;
    }
  }
  if(!isValid){
    throw 'Le role n\'est pas valide';
  }
}
