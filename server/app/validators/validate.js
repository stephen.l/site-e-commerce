const {validationResult} = require('express-validator');

// Vérifie si il y a des erreurs après n'importe quel validateur, s'il n'y en pas, validate envoie sur la fonction suivante

// Verify if there is errors after any validators, if not, validate send to the next function

exports.validate = (req, res, next) => {

  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }
  const extractedErrors = [];
  errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }));

  return res.status(422).json({
    errors: extractedErrors,
  });
}
