const {check} = require('express-validator');
const db = require('../../db/pg');

exports.validateCreate = () => {
  return[
    check('id_client')
      .notEmpty().withMessage('L\'id client est requis')
      .custom(clientExist)
  ];
}

exports.validateUpdate = () => {
  return [
    check('status')
      .notEmpty().withMessage("Le status est requis")
      .custom(checkStatusValue)
  ];
}

const clientExist = async (value) => {
  const result = await db.query({
    text: 'select id from clients where id=$1',
    values: [value],
    rowMode: 'array'
  });

  if(result.rows.length!=1){
    throw 'L\'id client n\'existe pas';
  }
}

const checkStatusValue = async (value) => {
  const result = await db.query({
    text: 'SELECT unnest(enum_range(NULL::status)) as status',
    rowMode: "array"
  });
  let isValid = false;

  for (r of result.rows) {
    if(String(r)===String(value)){
      isValid = true;
    }
  }
  if(!isValid){
    throw 'Le status n\'est pas valide';
  }
}
