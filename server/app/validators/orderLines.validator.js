const {check} = require('express-validator');
const db = require('../../db/pg');

exports.validateCreate = () => {
  return [
    check('order_ref')
      .notEmpty().withMessage('La ref de la commande est requise')
      .custom(orderExist)
      .custom(primaryKeyIsUnique),

    check('product_ref')
      .notEmpty().withMessage('La ref produit est requise')
      .custom(productExist),

    check('quantity')
      .notEmpty().withMessage('La quantité est requise')
      .custom((value) => value > 0).withMessage('La quantité doit être positive')
  ];
}

exports.validateUpdate = () => {
  return [
    check('quantity')
      .notEmpty().withMessage('La quantité est requise')
      .custom((value) => value > 0).withMessage('La quantité doit être positive')
  ];
}

const primaryKeyIsUnique = async (value,{req}) => {
  const result = await db.query({
    text: 'select * from order_lines where order_ref=$1 and product_ref=$2',
    values: [req.body.order_ref,req.body.product_ref],
    rowMode: 'array'
  });

  if(result.rows.length!=0){
    throw 'Ce produit existe déjà dans cette commande';
  }
}

const orderExist = async (value) => {
  const result = await db.query({
    text: 'select ref from orders where ref=$1',
    values: [value],
    rowMode: 'array'
  });

  if(result.rows.length!=1){
    throw 'La ref du la commande n\'existe pas';
  }
}

const productExist = async (value) => {
  const result = await db.query({
    text: 'select ref from products where ref=$1',
    values: [value],
    rowMode: 'array'
  });

  if(result.rows.length!=1){
    throw 'La ref du produit n\'existe pas';
  }
}
