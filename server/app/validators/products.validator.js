const {check} = require('express-validator');

exports.validateProduct = () => {
  return [
    check('name').notEmpty().withMessage("Le nom est requis"),
    check('stock')
      .notEmpty().withMessage("Le stock est requis")
      .custom((value) => value >= 0).withMessage("Le stock doit être positif"),
    check('stock_alert')
      .notEmpty().withMessage("Le stock alerte est requis")
      .custom((value) => value >= 0).withMessage("Le stock alerte doit être positif"),
    check('price')
      .notEmpty().withMessage("Le prix est requis")
      .custom((value) => value >= 0).withMessage("Le prix doit être positif")
  ];
}
