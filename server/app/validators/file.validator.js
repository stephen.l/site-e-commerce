exports.fileExists = (req, res, next) => {
    if (req.files!=null) {
      next();
    }
    else {
      res.status(422).json("L'image est requise !");
    }
}
