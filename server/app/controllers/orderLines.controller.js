const db = require('../../db/pg');
const axios = require('axios').default;

const port = process.env.PORT || 8000;
axios.defaults.baseURL = 'http://localhost:'+port;

exports.create = async (req,res) => {
  try {
    const {order_ref,product_ref,quantity} = req.body;

    const product = await db.query('select * from products where ref = $1',[product_ref]);

    if (product.rows[0].stock < quantity) {
      await res.status(422).json(`quantité du produit ${product.rows[0].name} indisponible`);
    }
    else {
      const {price, price_with_vat} = await refreshLinePrice(product_ref,quantity).then(res => res);

      await db.query('insert into order_lines values ( $1,$2,$3,$4,$5)',[order_ref,product_ref,quantity,price,price_with_vat]);

      await refreshTotalPrice(order_ref);
      await updatePopularity(product_ref, quantity);
      await res.status(200).json("OrderLine created successfully");
    }
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

exports.index = async (req,res) => {
  try {
    const result = await db.query('select * from order_lines');
    await res.status(200).json(result.rows);
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

exports.show = async (req,res) => {
  try {
    const result = await db.query('select * from order_lines where order_ref = $1 and product_ref=$2 ',[req.params.order_ref,req.params.product_ref]);
    await res.status(200).json(result.rows);
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

exports.showAllLines = async(req,res) => {
  try {
    const result = await db.query('select * from order_lines where order_ref = $1',[req.params.order_ref]);
    await res.status(200).json(result.rows);
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

exports.update = async (req,res) => {
  try {
    const {quantity} = req.body;
    const {price, price_with_vat} = await refreshLinePrice(req.params.product_ref,quantity).then(res => res);

    await db.query('update order_lines set quantity = $1, price = $2, price_with_vat=$3 where order_ref = $4 and product_ref=$5',[quantity,price,price_with_vat,req.params.order_ref,req.params.product_ref]);
    await res.status(200).json("OrderLine updated successfully");
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

exports.delete = async (req,res) => {
  try {
    await db.query('delete from order_lines where order_ref = $1 and product_ref=$2',[req.params.order_ref,req.params.product_ref]);
    await refreshTotalPrice(req.params.order_ref);
    await res.status(200).json("OrderLine deleted successfully");
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

const refreshLinePrice = async (productRef,quantity) => {
  const product = await axios.get(`/products/${productRef}`);
  const price = quantity * product.data[0].price;
  const price_with_vat = price * 1.20;

  return {price: price, price_with_vat: price_with_vat};
}

const refreshTotalPrice = async (orderRef) => {
  const allOrderLines = await db.query({
        text: 'select price_with_vat from order_lines where order_ref = $1',
      values: [orderRef],
      rowMode: "array"
      }
  );
  let total = 0;
  for (let ol of allOrderLines.rows){
    total += ol[0];
  }

  await db.query('update orders set total_price = $1 where ref = $2',[total,orderRef]);
}

const updatePopularity = async (productRef, quantity) => {
  await db.query('update products set popularity = popularity + 1, stock = stock - $1 where ref = $2',[quantity,productRef]);
}
