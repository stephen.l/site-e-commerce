const db = require('../../db/pg');
const {validationResult} = require('express-validator');
const bcrypt = require('bcrypt');

exports.create = async (req, res) => {
    try {
        const { last_name, first_name, email, address } = req.body;
        const hashedPasword = await bcrypt.hash(req.body.password, 10);

        await db.query('insert into clients values (default, $1, $2, $3, $4, $5, $6)',[last_name, first_name, email, hashedPasword, 'client', address]);
        await res.status(200).json("Client created successfully");
    }
    catch (err) {
        await res.status(500).json("Erreur interne au serveur");;
    }
}

exports.index = async (req, res) => {
    try {
      const offset = req.params.offset ? req.params.offset :0;
      const text = req.params.limit ?
        'select id,first_name,last_name,email,role,address from clients order by id limit '+req.params.limit+' offset $1'
        :
        'select id,first_name,last_name,email,role,address from clients order by id offset $1';

      const result = await db.query(text,[offset]);
        await res.status(200).json(result.rows);
    }
    catch (err) {
        await res.status(500).json("Erreur interne au serveur");
    }
}

exports.show = async (req, res) => {
    try {
        const result = await db.query('select id,first_name,last_name,email,role,address from clients where id = $1', [req.params.id]);
        await res.status(200).json(result.rows);
    }
    catch (err) {
        await res.status(500).json("Erreur interne au serveur");
    }
}

exports.showOrders = async (req,res) => {
    try {
      const offset = req.params.offset ? req.params.offset :0;
      const text = req.params.limit ?
        'select * from orders where id_client = $1 order by ref desc limit '+req.params.limit+' offset $2'
        :
        'select * from orders where id_client = $1 order by ref desc offset $2';
      const result = await db.query(text,[req.params.id,offset]);
        res.status(200).json(result.rows);
    }
    catch (err) {
        await res.status(500).json("Erreur interne au serveur");
    }
}


exports.update = async (req, res) => {
    try {
      const { last_name, first_name, email, role, password, address } = req.body;

      if(password){
        const hashedPasword = await bcrypt.hash(req.body.password, 10);

        await db.query('update clients set last_name = $1, first_name = $2, email = $3, password = $4, address = $5, role = $6 where id = $7',[last_name, first_name, email, hashedPasword, address, role, req.params.id]);
      }
      else {
        await db.query('update clients set last_name = $1, first_name = $2, email = $3,address = $4, role = $5 where id = $6',[last_name, first_name, email, address, role, req.params.id]);
      }
      // res.redirect('/clients');
      res.json(200,"Client updated successfully");
    }
    catch (err) {
        await res.status(500).json("Erreur interne au serveur");
    }
}

exports.delete = async (req, res) => {
    try {
        await db.query('delete from clients where id = $1',[parseInt(req.params.id)]);
        await res.status(200).json("Client deleted successfully");
    }
    catch (err) {
        await res.status(500).json("Erreur interne au serveur");
    }
}

exports.getRoles = async (req,res) => {
  try {
    const result = await db.query({
      text: 'SELECT unnest(enum_range(NULL::role)) as roles',
      rowMode: "array"
    });
    await res.status(200).json(result.rows);
  } catch (e) {
    await res.status(500).json("Erreur interne au serveur");
  }
}
