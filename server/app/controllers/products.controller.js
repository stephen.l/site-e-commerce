const db = require('../../db/pg');

exports.create = async (req,res) => {
  try {
    const {name,description,stock,stock_alert,price} = req.body;
    const file = req.files.file;

    const ext = file.mimetype.split("/")[1];

    file.mv(`${__dirname}/../../public/img/${name}.${ext}`,err => {
      if (err) {
        console.error(err);
        return res.status(500).send(err);
      }
    });

    await db.query('insert into products values (default, $1,$2,$3,$4,$5,$6,$7)',[name,description,stock,stock_alert,price,0,"img/"+name+"."+ext]);

    await res.status(200).json("Product created successfully");
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

exports.index = async (req,res) => {
  try {
    const offset = req.params.offset ? req.params.offset : 0;
    const text = req.params.limit ?
      'select * from products limit '+req.params.limit+' offset $1'
      :
      'select * from products offset $1';
    console.log(text+" "+offset);
    const result = await db.query(text,[offset]);
    await res.status(200).json(result.rows);
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

exports.bestSales = async (req,res) => {
  try {
    const offset = req.params.offset ? req.params.offset : 0;
    const text = req.params.limit ?
      'select * from products  order by popularity desc limit '+req.params.limit+' offset $1'
      :
      'select * from products order by popularity desc offset $1';
    const result = await db.query(text,[offset]);
    await res.status(200).json(result.rows);
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

exports.show = async (req,res) => {
  try {
    const result = await db.query('select * from products where ref = $1',[req.params.ref]);
    await res.status(200).json(result.rows);
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

exports.update = async (req,res) => {
  try {
    if (req.files!==null) {
      const { name,description,stock,stock_alert, price} = req.body;
      const file = req.files.file;

      const ext = file.mimetype.split("/")[1];

      file.mv(`${__dirname}/../../public/img/${name}.${ext}`,err => {
        if (err) {
          console.error(err);
          return res.status(500).send(err);
        }
      });

      await db.query('update products set name = $1, description = $2, stock = $3, stock_alert = $4, price = $5, url_img= $7 where ref = $6',[name,description,stock,stock_alert,price,req.params.ref,"img/"+name+"."+ext]);
      await res.status(200).json("Product updated successfully");
    }
    else {
      const { name,description,stock,stock_alert, price} = req.body;
      await db.query('update products set name = $1, description = $2, stock = $3, stock_alert = $4, price = $5 where ref = $6',[name,description,stock,stock_alert,price,req.params.ref]);
      await res.status(200).json("Product updated successfully");
    }

  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

exports.delete = async (req,res) => {
  try {
    await db.query('delete from products where ref = $1',[req.params.ref]);
    await res.status(200).json("Product deleted successfully");
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}
