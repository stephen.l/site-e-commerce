const db = require('../../db/pg');
const axios = require('axios').default;


exports.create = async (req,res) => {
  try {
    const {id_client, address} = req.body;
    await db.query('insert into orders values (default, $1,$2,$3,$4)',[id_client,0,'passé',address]);
    const result = await db.query('select ref from orders where id_client=$1 order by ref desc limit 1',[id_client]);
    console.log(result.rows);
    await res.status(200).json(result.rows);
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

exports.index = async (req,res) => {
  try {
    const offset = req.params.offset ? req.params.offset : 0;
    const text = req.params.limit ?
    'select * from orders order by ref desc limit '+req.params.limit+' offset $1'
    :
    'select * from orders order by ref desc offset $1';

    const result = await db.query(text,[offset]);
    await res.status(200).json(result.rows);
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

exports.show = async (req,res) => {
  try {
    const result = await db.query('select * from orders where ref = $1',[req.params.ref]);
    await res.status(200).json(result.rows);
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

exports.update = async (req,res) => {
  try {
    const {status} = req.body;
    await db.query('update orders set status = $1 where ref = $2',[status,req.params.ref]);
    await res.status(200).json("Order updated successfully");
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

exports.delete = async (req,res) => {
  try {
    await db.query('delete from orders where ref = $1',[req.params.ref]);
    await res.status(200).json("Order deleted successfully");
  }
  catch (err) {
    await res.status(500).json("Erreur interne au serveur");
  }
}

exports.getStatus = async (req,res) => {
  try {
    const result = await db.query({
      text: 'SELECT unnest(enum_range(NULL::status)) as status',
      rowMode: "array"
    });
    await res.status(200).json(result.rows);
  } catch (e) {
    await res.status(500).json("Erreur interne au serveur");
  }
}
