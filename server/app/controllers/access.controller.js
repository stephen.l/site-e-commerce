const jwt = require('jsonwebtoken');

exports.isAdmin = (req, res, next) => {
    const tokenArray = req.headers.authorization.split(" ");
    const payload = jwt.decode(tokenArray[1]);

    if (payload.role === 'admin'){
        next();
    } else {
        res.status(401).json("Vous n'avez pas accès à cette page");
    }
}

exports.isSeller = (req, res, next) => {
    const tokenArray = req.headers.authorization.split(" ");
    const payload = jwt.decode(tokenArray[1]);

    if (payload.role === 'seller'){
        next();
    } else {
        res.status(401).json("Vous n'avez pas accès à cette page");
    }
}

exports.isAdminOrHimSelf = (req, res, next) => {
    const tokenArray = req.headers.authorization.split(" ");
    const payload = jwt.decode(tokenArray[1]);

    if (payload.role === 'admin'){
        next();
    }
    else if (Number(payload.id) === Number(req.params.id)) {
        next();
    }
    else {
        rres.status(401).json("Vous n'avez pas accès à cette page");
    }
}
