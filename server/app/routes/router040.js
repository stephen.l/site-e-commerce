const express = require("express");
const router = express.Router();

const productsController = require('../controllers/products.controller');
const clientsController = require('../controllers/clients.controller');
const ordersController =  require('../controllers/orders.controller');
const ordersLineController =  require('../controllers/orderLines.controller');

router
  .get("/", (req, res) => {
       res.json("Hello world !!");
   })
   .get('/clients',clientsController.index)
   .post('/clients/create',clientsController.create)
   .put('/clients/update/:id',clientsController.update)
   .delete('/clients/delete/:id',clientsController.delete)
   .get('/clients/:id',clientsController.show)

   .get('/products',productsController.index)
   .post('/products/create',productsController.create)
   .put('/products/update/:ref',productsController.update)
   .delete('/products/delete/:ref',productsController.delete)
   .get('/products/:ref',productsController.show)

   .get('/orders',ordersController.index)
   .post('/orders/create',ordersController.create)
   .put('/orders/update/:ref',ordersController.update)
   .delete('/orders/delete/:ref',ordersController.delete)
   .get('/orders/:ref',ordersController.show)

   .get('/ordersLine',ordersLineController.index)
   .post('/ordersLine/create',ordersLineController.create)
   .put('/ordersLine/update/:orderRef-:productRef',ordersLineController.update)
   .delete('/ordersLine/delete/:orderRef-:productRef',ordersLineController.delete)
   .get('/ordersLine/:orderRef-:productRef',ordersLineController.show)



router
  .use((req, res) => {
          res.status(404);
          res.json({
              error: "Page not found"
          });
      });

module.exports = router;
