const express = require("express");
const router = express.Router();
const passport = require("passport");

// The controllers
const productsController = require('../controllers/products.controller');
const clientsController = require('../controllers/clients.controller');
const ordersController =  require('../controllers/orders.controller');
const orderLinesController =  require('../controllers/orderLines.controller');
const accessController =  require('../controllers/access.controller');

// Validate
const { validate } = require('../validators/validate');

// Validators
const clientsValidator = require('../validators/clients.validator');
const productsValidator = require('../validators/products.validator');
const ordersValidator = require('../validators/orders.validator');
const orderLinesValidator = require('../validators/orderLines.validator');
const fileValidator =  require('../validators/file.validator');

const {authenticateUser} = require('../../authentificate/authenticateUser');

router
    .get("/", (req, res) => {
        res.json("Hello world !!");
    })

    .post("/login", authenticateUser)
    .get("/roles",
    passport.authenticate('jwt',{session: false}),
    accessController.isAdmin,
    clientsController.getRoles
    )
    .get("/status",
    passport.authenticate('jwt',{session: false}),
    accessController.isSeller,
    ordersController.getStatus
    )
    .get(
      '/clients',
      passport.authenticate('jwt',{session: false}),
      accessController.isAdmin,
      clientsController.index
    )
    .get(
      '/clients/limit=:limit?-offset=:offset?',
      passport.authenticate('jwt',{session: false}),
      accessController.isAdmin,
      clientsController.index
    )
    .post(
      '/register',
      clientsValidator.validateCreate(),
      validate,clientsController.create
    )
    .put(
      '/clients/:id',
      passport.authenticate('jwt',{session: false}),
      accessController.isAdminOrHimSelf,
      clientsValidator.validateUpdate(),
      validate,
      clientsController.update
    )
    .delete(
      '/clients/:id',
      passport.authenticate('jwt',{session: false}),
      accessController.isAdminOrHimSelf,
      clientsController.delete
    )
    .get(
      '/clients/:id',
      passport.authenticate('jwt',{session: false}),
      clientsController.show
    )
    .get(
      '/clients/:id/orders/limit=:limit?-offset=:offset?',
      passport.authenticate('jwt',{session: false}),
      clientsController.showOrders
    )
    .get(
      '/clients/:id/orders',
      passport.authenticate('jwt',{session: false}),
      clientsController.showOrders
    )

    .get('/products',productsController.index)
    .get('/products/limit=:limit?-offset=:offset?',productsController.index)
    .post(
      '/products',
      passport.authenticate('jwt',{session: false}),
      accessController.isSeller,
      productsValidator.validateProduct(),
      fileValidator.fileExists,
      validate,
      productsController.create
    )
    .put(
      '/products/:ref',
      passport.authenticate('jwt',{session: false}),
      accessController.isSeller,
      productsValidator.validateProduct(),
      validate,
      productsController.update
    )
    .delete(
      '/products/:ref',
      passport.authenticate('jwt',{session: false}),
      accessController.isSeller,
      productsController.delete
    )
    .get('/products/best-sales',productsController.bestSales)
    .get('/products/best-sales/limit=:limit?-offset=:offset?',productsController.bestSales)
    .get('/products/:ref',productsController.show)

    .get('/orders',ordersController.index)
    .get('/orders/limit=:limit?-offset=:offset?',ordersController.index)
    .post(
      '/orders',
      ordersValidator.validateCreate(),
      validate,
      ordersController.create
    )
    .put(
      '/orders/:ref',
      ordersValidator.validateUpdate(),
      validate,
      ordersController.update
    )
    .delete('/orders/:ref',ordersController.delete)
    .get('/orders/:ref',ordersController.show)

    .get('/order-lines',orderLinesController.index)
    .post(
      '/order-lines',
      orderLinesValidator.validateCreate(),
      validate,
      orderLinesController.create
    )
    .put(
      '/order-lines/:order_ref-:product_ref',
      orderLinesValidator.validateUpdate(),
      validate,
      orderLinesController.update
    )
    .delete('/order-lines/:order_ref-:product_ref',orderLinesController.delete)
    .get('/order-lines/:order_ref-:product_ref',orderLinesController.show)
    .get('/order-lines/:order_ref',orderLinesController.showAllLines)



router
    .use((req, res) => {
        res.status(404);
        res.json({
            error: "Page not found"
        });
    });

module.exports = router;
