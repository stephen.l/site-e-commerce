const express = require("express");
const router = express.Router();

// The controllers
const productsController = require('../controllers/products.controller');
const clientsController = require('../controllers/clients.controller');
const ordersController =  require('../controllers/orders.controller');
const orderLinesController =  require('../controllers/orderLines.controller');

// Validate
const { validate } = require('../validators/validate');

// Validators
const clientsValidator = require('../validators/clients.validator');
const productsValidator = require('../validators/products.validator');
const ordersValidator = require('../validators/orders.validator');
const orderLinesValidator = require('../validators/orderLines.validator');

router
  .get("/", (req, res) => {
       res.json("Hello world !!");
   })
   // Client routing
   .get('/clients',clientsController.index)
   .post(
     '/clients/create',
     clientsValidator.validateCreate(),
     validate,
     clientsController.create
   )
   .put(
     '/clients/update/:id',
     clientsValidator.validateUpdate(),
     validate,
     clientsController.update
   )
   .delete('/clients/delete/:id',clientsController.delete)
   .get('/clients/:id',clientsController.show)

   // Products routing
   .get('/products',productsController.index)
   .post(
     '/products/create',
     productsValidator.validateProduct(),
     validate,
     productsController.create
   )
   .put(
     '/products/update/:ref',
     productsValidator.validateProduct(),
     validate,
     productsController.update
   )
   .delete('/products/delete/:ref',productsController.delete)
   .get('/products/:ref',productsController.show)

   // orders routing
   .get('/orders',ordersController.index)
   .post(
     '/orders/create',
     ordersValidator.validateCreate(),
     validate,
     ordersController.create
   )
   .put(
     '/orders/update/:ref',
     ordersValidator.validateUpdate(),
     validate,
     ordersController.update
   )
   .delete('/orders/delete/:ref',ordersController.delete)
   .get('/orders/:ref',ordersController.show)

   // order-lines routing
   .get('/order-lines',orderLinesController.index)
   .post(
     '/order-lines/create',
     orderLinesValidator.validateCreate(),
     validate,
     orderLinesController.create
   )
   .put(
     '/order-lines/update/:order_ref-:product_ref',
     orderLinesValidator.validateUpdate(),
     validate,
     orderLinesController.update
   )
   .delete('/order-lines/delete/:order_ref-:product_ref',orderLinesController.delete)
   .get('/order-lines/:order_ref-:product_ref',orderLinesController.show)
   .get('/order-lines/:order_ref',orderLinesController.showAllLines)



router
  .use((req, res) => {
          res.status(404);
          res.json({
              error: "Page not found"
          });
      });

module.exports = router;
