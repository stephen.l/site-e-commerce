const express = require("express");
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require("body-parser");
const passport = require("passport");
const fileUpload = require("express-fileupload");

const initializePassport = require("./authentificate/passportConfig");

initializePassport(passport);


const routerV040 = require('./app/routes/router040');
const routerV120 = require('./app/routes/router120');
const routerV211 = require('./app/routes/router211');

const app = express();
const port = process.env.PORT || 8000;

app.use(morgan('combined'));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
        extended: true
    }));

app.use(passport.initialize());

app.use(express.static('public'));

app.use(fileUpload());;

app.use('/0.1.0',routerV040);
app.use('/0.2.0',routerV040);
app.use('/0.2.1',routerV040); // même router car bug était dans la db
app.use('/0.3.0',routerV040);
app.use('/0.4.0',routerV040);
app.use('/0.4.1',routerV040); // passe par le même router car changement à la DB
app.use('/1.0.0',routerV120);
app.use('/1.0.1',routerV120);
app.use('/1.0.2',routerV120);
app.use('/1.1.0',routerV120);
app.use('/1.1.1',routerV120);
app.use('/1.2.0',routerV120);
app.use('/2.0.0',routerV211);
app.use('/2.0.1',routerV211);
app.use('/2.1.1',routerV211);

app.use(routerV211); // Requests processing will be defined in the file router
app.listen(port, () => console.log('Server app listening on port ' + port));
