const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const db = require('../db/pg');
const config = require('./config');

const authenticateUser = async (req,res) => {
        const result = await db.query({
          text: 'Select * from clients WHERE email = $1',
          values: [req.body.email]
        });
        if (result.rows.length===0) {
            res.status(401).json("Le mail ne correspond pas");
        }
        else{
          const user = result.rows[0];

          bcrypt.compare(req.body.password, user.password, (err,isMatch) => {
              if (err){
                  res.status(401).json("Le mot de passe ne correspond pas");}

              if (isMatch) {
                // Generate token here
                const payload = {
                    id: user.id,
                    role: user.role
                };

                const token = jwt.sign(payload, config.jwtSecret, { expiresIn: '4h' });
                res.json({
                    token: token
                });
              }
              else {
                  res.status(401).json("Le mot de passe ne correspond pas");
              }
          });
        }
};

module.exports = {
  authenticateUser
};
