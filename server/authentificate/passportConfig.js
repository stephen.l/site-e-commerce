const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const bcrypt = require('bcrypt');

const db = require('../db/pg');
const config = require('./config');

const params = {
    secretOrKey: config.jwtSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
}

function initialize (passport) {
    const strategy = new JwtStrategy(params, (payload, done) => {
        const user = db.query('select * from clients where id = $1',[payload.id]);
        
        if (user){
            return done(null, {id: user.id, role: user.role});
        } else {
            return done(new Error("User not found"), null);
        }
    });
    passport.use(strategy);
}

module.exports = initialize;
