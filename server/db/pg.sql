drop schema if exists projet cascade;
create schema projet;
set search_path to projet;

create type role AS ENUM ('client', 'seller', 'admin');

create table if not exists clients
(
    id serial primary key,
    last_name varchar not null,
    first_name varchar not null,
    email varchar unique not null,
    password varchar not null,
    role role not null,
    address varchar

);

insert into clients values
    (default,'Laurent','Maxence','admin@mail.com','$2b$10$ZFSK2Esl7PXawjpNRhFh3.8gW6Lasz1K9vsj7VAQIFJ/IQM1LXyim','admin','1 route de Lille, Lens 62300'),
    (default,'Legras','Stephen','seller@mail.com','$2b$10$ZFSK2Esl7PXawjpNRhFh3.8gW6Lasz1K9vsj7VAQIFJ/IQM1LXyim','seller','1 route de Béthune, Lens 62300'),
    (default,'Malbranque','Illiade','client@mail.com','$2b$10$ZFSK2Esl7PXawjpNRhFh3.8gW6Lasz1K9vsj7VAQIFJ/IQM1LXyim','client','1 rue Raoult Briquet, Lens 62300'),
    (default,'Second','client','client2@mail.com','$2b$10$ZFSK2Esl7PXawjpNRhFh3.8gW6Lasz1K9vsj7VAQIFJ/IQM1LXyim','client',null);


create table if not exists products
(
    ref serial primary key,
    name varchar not null,
    description varchar,
    stock int not null,
    stock_alert int not null,
    price float not null,
    popularity int not null,
    url_img varchar not null
);

insert into products values
  (default,'Pull Blanc','Pull blanc qui gratte un peu',10,4,25,5,'img/Pull Blanc.jpg'),
  (default,'Pull Col V','Pull col V de couleurs gris foncé. 100% cotton.',34,10,23,12,'img/Pull Col V.jpg'),
  (default,'Pull Nike','Pull Nike de couleur bleu foncé. 100% cotton',34,10,50,45,'img/Pull Nike.jpg'),
  (default,'Ralph Lauren Pull Violet','Pull Ralph Laurent de couleur Violet. Idéal pour une session de golf. 100% cotton',34,10,23,33,'img/Ralph Lauren Pull Violet.jpg'),
  (default,'MSI Prestige','MSI Prestige, pc portable 15" disposant des derniers composants en date. Inter i5-10300u et Nvidia GTX 2030.',34,10,1500,33,'img/MSI Prestige.jpg'),
  (default,'Barres Protéinée','lot de 6 barres protéinée ',34,12,4,34,'img/Barres Protéinée.png'),
  (default,'Chauffage de Voiture','Petit chauffage de voiture, idéal pour ceux dont la voiture ne possède plus de chauffage',2,3,9.99,5,'img/Chauffage Voiture.jpg'),
  (default,'Lacoste Polo Gris','Petit polo Lacoste de couleur grise',105,54,32,25,'img/Lacoste Polo Gris.jpeg'),
  (default,'Leninade','Servis bien fraîche, cette citronnade vous laissera un goût de communisme en plus du goût citronné',123,145,2.42,33,'img/Leninade.JPG'),
  (default,'Parka','Petite parka pour vous protéger du mauvais temps',0,4,12,6,'img/Parka.jpeg'),
  (default,'Marcel','Marcel possédant un blanc étincellant',34,24,23,21,'img/Marcel.jpeg'),
  (default,'Batterie Externe Asus','Batterie Externe Asus de 8000 mhA.',35,50,35,35,'img/Batterie Externe Asus.jpg');

create type status AS ENUM ('annulé', 'passé', 'en préparation', 'envoyé');

create table if not exists orders
(
  ref serial primary key,
  id_client int not null references clients(id) on update cascade on delete cascade,
  total_price float not null,
  status status not null,
  address varchar not null
);

insert into orders values
  (default,3,30,'annulé','1 rue Raoult Briquet, Lens 62300'),
  (default,3,60,'en préparation','1 rue Raoult Briquet, Lens 62300'),
  (default,3,4.8,'passé','1 rue Raoult Briquet, Lens 62300'),
  (default,3,69.8,'envoyé','1 rue Raoult Briquet, Lens 62300'),
  (default,4,1800,'envoyé','1 rue de la Soif, Lille');

create table if not exists order_lines
(
  order_ref int not null references orders(ref)  on update cascade on delete cascade,
  product_ref int not null references products(ref) on update cascade on delete cascade,
  quantity int not null,
  price float,
  price_with_vat float,
  primary key (order_ref,product_ref)
);

insert into order_lines values
  (1,7,1,25,30),
  (2,9,1,50,60),
  (3,1,1,4,4.8),
  (4,10,1,23,27.6),
  (4,12,1,35,42),
  (5,11,1,1500,1800);
