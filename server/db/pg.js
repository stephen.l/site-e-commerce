const {Pool} = require('pg');
const pool = new Pool();

pool.on('connect', client => {
    client.query('set search_path to projet')
});

module.exports = {
    query: (text, params) => {
        return pool.query(text, params)
    }
};
