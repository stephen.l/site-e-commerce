# Projet e-Commerce

Projet de fin d'étude du DUT Info

Le  but  de  ce  projet  est  de  créer  une  application  de  commerce  en  ligne  à  l’aide  du framework JavaScript  [React.js](https://fr.reactjs.org/)  pour  la  partie front-end,  et  du framework [Express.js](http://expressjs.com/) pour la partie back-end.

## Tester le projet

Depuis la racine du projet, le package [Concurrently](https://www.npmjs.com/package/concurrently) permet de lancer des commandes en simultanées et donc d'installer ou de lancer le projet en une commande

### Installer les dépendances :

`npm install`

### Créer la base de données :

Vous devez avoir installer [postgresql](https://www.postgresql.org/) et des variables d'environnement correctes.

`psql -f server/db/pg.sql`

### Lancer le projet :

Depuis la racine du projet

`npm start`

Le site est visible sur **localhost:3000** et le serveur est maintenant disponible sur localhost:8000 ou sur l'adresse configuré.

### Se connecter sur le client :

Pour vous connecter sur le site il existe 3 comptes ayant chacun un rôle différent.

`admin@mail.com` vous donnera l’accès d'un administrateur.

`seller@mail.com` vous donnera l’accès d'un vendeur.

`client@mail.com` vous donnera l’accès d'un client.

Le mot de passe par défaut est `lemotdepasse`.

## Configurer le projet :

### Configurer le secret pour JWT :

Le secret pour le token JWT se trouve dans `server/authenticate/config.js`

### Changer l'url du serveur sur React :

L'url du serveur auquel se connecte le client React se trouve dans `client/src/serverUrl.js`
